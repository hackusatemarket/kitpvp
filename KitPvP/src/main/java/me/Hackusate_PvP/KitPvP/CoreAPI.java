package me.Hackusate_PvP.KitPvP;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.*;

public class CoreAPI {

    private static Set<UUID> tpcooldown = new HashSet<>();
    private static final Set<UUID> deathm = new HashSet<UUID>();
    private static final Set<UUID> broadcastm = new HashSet<UUID>();
    public static boolean isTpCooldown(Player player) {
        return tpcooldown.contains(player.getUniqueId());
    }
    private static final Set<UUID> queue = new HashSet<UUID>();
    private static Boolean ffaenable = true;
    private static Set<UUID> ffaplayers = new HashSet<UUID>();
    private final Set<String> kittype = new HashSet<String>();
    private static Set<UUID> ffawiner = new HashSet<UUID>();
    private static Map<UUID, Integer> ffawins = new HashMap<UUID, Integer>();
    public static String kit;
    public static int pvp;
    public static Set<UUID> team1 = new HashSet<UUID>();
    public static Set<UUID> team2 = new HashSet<UUID>();
    public static int start;
    public static int joined;
    public static int tdm;
    public static Set<UUID> fly = new  HashSet<>();
    public static Map<UUID, Integer> streak = new HashMap<>();

    public static void setTpCooldown(Player player, boolean status) {
        if (status) {
            tpcooldown.add(player.getUniqueId());
        } else {
            tpcooldown.remove(player.getUniqueId());
        }
    }

    public static Integer getStreak(Player player) {
        return streak.get(player.getUniqueId());
    }

    public static Integer addStreak(Player player) {
        return streak.put(player.getUniqueId(), streak.get(player.getUniqueId())+ 1);
    }

    public static boolean hasDeathDisabled(Player player) {
        return deathm.contains(player.getUniqueId());
    }

    public static boolean hasBroadcastDisabled(Player player) {
        return broadcastm.contains(player.getUniqueId());
    }

    public static void setBroadcastMessages(Player player, boolean status) {
        UUID uuid = player.getUniqueId();
        if (!(status)) {
            broadcastm.add(uuid);
        } else {
            broadcastm.remove(uuid);
        }
    }


    public static void setDeathMessages(Player player, boolean status) {
        UUID uuid = player.getUniqueId();
        if (!(status)) {
            deathm.add(uuid);
        } else {
            deathm.remove(uuid);
        }
    }

    public boolean isInQueue(Player player) {
        return queue.contains(player.getUniqueId());
    }

    public static boolean isFFAEnabled() {
        if (ffaenable) {
            return true;
        } else {
            return false;
        }

    }


    public static Integer getJoinedPlayer() {
        return ffaplayers.size();
    }

    public static Set<UUID> getJoinedPlayers() {

        return ffaplayers;
    }

    public boolean isJoined(Player player) {
        return ffaplayers.contains(player.getUniqueId());
    }

    public static void setJoined(Player player) {
        ffaplayers.add(player.getUniqueId());
    }


    public static void clearList() {
        ffaplayers.clear();
    }


    public static boolean isJoinedPlayer(Player player) {
        return ffaplayers.contains(player.getUniqueId());
    }

    public static Integer getMaxPlayers() {
        return Main.config.getInt("Event.FFA.Max");
    }

    private static String translate(Player player, String path) {
        if (path.contains("%player%")) {
            path = path.replace("%player%", String.valueOf(player.getName()));
        }

        return path;

    }

    public static Integer addWin(Player player) {
        int wins = Main.config.getInt("Storage.Players." + player.getName() + ".kills");
        return wins + 1;
    }

    public static Integer getWins(Player player) {
        return Main.config.getInt("Storage.Players." + player.getName() + ".wins");
    }

    public static void setWiiner(Player player) {
        ffawiner.add(player.getUniqueId());
    }

    public static Boolean isWinner(Player player) {
        return ffawiner.contains(player.getUniqueId());
    }


    public static void applyKit(Player player) {
        String kit = Main.config.getString("Executor.Command.Kit");
        kit = kit.replace("%player%", player.getName());
        kit = kit.replace("%kit%", kit);
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), kit);
    }

    public static void setQueue(Player player, boolean status, String kit) {
        Player online;
        Iterator var1;
        if (status) {
            if (kit.equals("NoDebuff")) {
                queue.add(player.getUniqueId());
                ;
                var1 = Bukkit.getOnlinePlayers().iterator();
                while (var1.hasNext()) {
                    online = (Player) var1.next();
                    if (getJoinedPlayer() >= Main.config.getInt("Event.FFA.Min") && getJoinedPlayer() <= Main.config.getInt("Event.FFA.Max")) {
                        if (isJoinedPlayer(online)) {

                            online.teleport(ffa());
                            String kitex = Main.config.getString("Executor.Command.Kit");
                            if (kitex.contains("%kit%") && kitex.contains("%player%")) {
                                kitex = kitex.replace("%kit%", String.valueOf(kit));
                                kitex = kitex.replace("%player%", String.valueOf(online.getName()));
                                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), kitex);
                                pvp = 1;
                            }
                        }
                    } else {
                        online.sendMessage(ChatColor.RED + "Not enough players.");
                        online.teleport(Main.getSpawn());
                        clearList();
                        queue.clear();
                        ffaplayers.clear();
                        kit = "";
                        pvp = 0;
                    }
                }
            }
        } else {
            queue.remove(player.getUniqueId());
        }
    }

    public static Location ffa() {

        Location ffa = new Location(Bukkit.getWorld(Main.config.getString("Arena.FFA.World")), Main.config.getInt("Arena.FFA.X"), Main.config.getInt("Arena.FFA.Y"), Main.config.getInt("Arena.FFA.Z"));
        return ffa;
    }


    public static boolean isTeam1(Player player) {
        UUID uuid = player.getUniqueId();
        return team1.contains(uuid);
    }

    public static boolean isTeam2(Player player) {
        UUID uuid = player.getUniqueId();
        return team2.contains(uuid);
    }

    public static void addTeam(Player player, Integer team) {
        UUID uuid = player.getUniqueId();
        if (team == 1) {
            if (!(isTeam1(player)) && !(isTeam2(player))) {
                team1.add(uuid);
                joined++;
            }
        } else if (team == 2) {
            if (!(isTeam1(player)) && !(isTeam2(player))) {
                team2.add(uuid);
                joined++;
            }
        }
    }

    public static Integer getTeam1Size() {
        return team1.size();
    }

    public static Integer getTeam2Size() {
        return team2.size();
    }

    public static Location getWaitingLobby() {
        return new Location(Bukkit.getWorld("world"), 0, 70, 0);
    }

    public static Integer getJoinedSize() {
        return joined;
    }


    public static void clearData() {
        team2.clear();
        team1.clear();
    }
}
