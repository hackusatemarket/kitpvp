package me.Hackusate_PvP.KitPvP.Settings;

import me.Hackusate_PvP.KitPvP.CoreAPI;
import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Inv implements Listener {
    public static Inventory settings = Bukkit.createInventory(null, 9, ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Name")));
    public static ItemStack dm = new ItemStack(Material.SKULL_ITEM);
    public static ItemMeta dmm = dm.getItemMeta();
    public static ItemStack broadcastm = new ItemStack(Material.ITEM_FRAME);
    public static ItemMeta broadcastmm = broadcastm.getItemMeta();


    @EventHandler
    public void build(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();



        if (e.getWhoClicked() != null) {
            if (e.getInventory() != null && e.getClickedInventory() != null) {
                if (e.getCurrentItem() != null) {
                    if (e.getCurrentItem().hasItemMeta()) {
                        if (e.getInventory().getName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Name")))) {
                            e.setCancelled(true);
                            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.DeathMessage.Name")))) {
                                e.setCancelled(true);
                                if (!(CoreAPI.hasDeathDisabled(p))) {
                                    p.sendMessage(ChatColor.RED + "You have disabled death messages.");
                                    CoreAPI.setDeathMessages(p, false);
                                    dmm.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.DeathMessage.Disabled"))));
                                    Inv.dm.setItemMeta(Inv.dmm);
                                    Inv.settings.setItem(0, Inv.dm);

                                } else {
                                    CoreAPI.setDeathMessages(p,true);
                                    p.sendMessage(ChatColor.GREEN + "You have enabled death messages.");
                                    dmm.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.DeathMessage.Enabled"))));
                                    Inv.dm.setItemMeta(Inv.dmm);
                                    Inv.settings.setItem(0, Inv.dm);
                                }




                            }

                            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.Broadcast.Name")))) {
                                if (!(CoreAPI.hasBroadcastDisabled(p))) {
                                    p.sendMessage(ChatColor.RED + "You have disabled broadcast messages.");
                                    CoreAPI.setBroadcastMessages(p,false);
                                    broadcastmm.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.Broadcast.Disabled"))));
                                    broadcastm.setItemMeta(broadcastmm);
                                    settings.setItem(1, broadcastm);
                                } else {
                                    p.sendMessage(ChatColor.GREEN + "You have enabled broadcast messages.");
                                    CoreAPI.setBroadcastMessages(p,true);
                                    broadcastmm.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.Broadcast.Enabled"))));
                                    broadcastm.setItemMeta(broadcastmm);
                                    settings.setItem(1, broadcastm);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
