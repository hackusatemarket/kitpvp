package me.Hackusate_PvP.KitPvP.Settings;

import me.Hackusate_PvP.KitPvP.CoreAPI;
import me.Hackusate_PvP.KitPvP.Main;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class Settings implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {
        Inv.dmm.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.DeathMessage.Name")));
        if (CoreAPI.hasDeathDisabled((Player) p)) {
            Inv.dmm.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.DeathMessage.Disabled"))));
        } else {
            Inv.dmm.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.DeathMessage.Enabled"))));
        }

        Inv.broadcastmm.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.Broadcast.Name")));
        if (CoreAPI.hasBroadcastDisabled((Player) p)) {
            Inv.broadcastmm.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.Broadcast.Disabled"))));
        } else {
            Inv.broadcastmm.setLore(Arrays.asList(ChatColor.translateAlternateColorCodes('&', Main.config.getString("GUI.Settings.Items.Broadcast.Enabled"))));
        }

        Inv.broadcastm.setItemMeta(Inv.broadcastmm);
        Inv.dm.setItemMeta(Inv.dmm);

        Inv.settings.setItem(0, Inv.dm);
        Inv.settings.setItem(1, Inv.broadcastm);


        if (p.hasPermission("core.command.settings")) {
            ((Player) p).openInventory(Inv.settings);
        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }


        return false;
    }
}
