package me.Hackusate_PvP.KitPvP.PvP;

import me.Hackusate_PvP.KitPvP.CoreAPI;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class DeathMessages implements Listener {


    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onDeatha(PlayerDeathEvent event) {
        Player player = event.getEntity();
        Player killer = event.getEntity().getKiller();

        if (killer != null) {
            String dem = Lang.DEATH_MESSAGE_KILLER_PLAYER.toString();
            dem = dem.replace("%player%", player.getName());
            dem = dem.replace("%killer%", killer.getName());
            event.setDeathMessage(null);
            for (Player players : Bukkit.getOnlinePlayers()) {
                if (!(CoreAPI.hasDeathDisabled(player))) {
                    player.sendMessage(dem);
                }
            }
        } else {
            String dem = Lang.DEATH_MESSAGE_KILLER_OTHER.toString();
            dem = dem.replace("%player%", player.getName());
            event.setDeathMessage(null);
            for (Player players : Bukkit.getOnlinePlayers()) {
                if (!(CoreAPI.hasDeathDisabled(player))) {
                    player.sendMessage(dem);
                }
            }
        }
    }
}
