package me.Hackusate_PvP.KitPvP.PvP;

import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class PearlCooldown implements Listener {
    private static Set<UUID> enderpearl= new HashSet<UUID>();

    public static boolean isCooldowned(Player player) {
        return enderpearl.contains(player.getUniqueId());
    }

    public static void setCooldown(Player player, boolean status) {
        if (status) {
            UUID uuid = player.getUniqueId();
            enderpearl.add(uuid);
        }
    }

    public void onPearl(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        if (event.getItem().getType() == Material.ENDER_PEARL) {
            setCooldown(player, true);
            if (enderpearl.contains(player)) {
               event.setCancelled(true);
                Main.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(Main.plugin, new Runnable() {
                    final int[] timer = {0};

                    @Override
                    public void run() {
                        if (isCooldowned(player)) {
                            if (timer[0] < 16) {
                                timer[0] = timer[0] + 1;
                                event.setCancelled(true);
                            }

                            if (timer[0] == 16) {
                                setCooldown(player, false);
                                player.sendMessage(ChatColor.GREEN + "You can now pearl again.");
                            }
                        }
                    }

                }, 0L, 20L);

            }
        }
    }
}
