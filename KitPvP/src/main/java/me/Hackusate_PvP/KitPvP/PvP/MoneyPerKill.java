package me.Hackusate_PvP.KitPvP.PvP;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class MoneyPerKill implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onKilla(PlayerDeathEvent event) {
        Player killed = event.getEntity();
        Player killer = event.getEntity().getKiller();
        if (killer != null) {
            int amount = Main.config.getInt("Settings.Death-Events.Money-Per-Kill");
            Main.getEconomy().depositPlayer(killer, amount);
            String msg = Lang.MONEY_EARNED_MESSAGE.toString();
            msg = msg.replace("%amount%", String.valueOf(amount));
            killer.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
        }
    }
}
