package me.Hackusate_PvP.KitPvP.PvP;

import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;

public class ArrowIndicater implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) {
            if (event.getCause().equals(EntityDamageEvent.DamageCause.PROJECTILE)) {
                Player victim = (Player) event.getEntity();
                Player attacker = ((Player) event.getEntity()).getKiller();
                if (victim != null) {
                    if (attacker != null) {
                        victim.sendMessage(ChatColor.GRAY + "You have been shot!");
                        attacker.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getAltColor() + "You have shot " + Main.getMainColor() + victim.getName() + Main.getAltColor() + "*" + Main.getOther() + event.getDamage()));
                    }
                }
            }
        }
    }
}
