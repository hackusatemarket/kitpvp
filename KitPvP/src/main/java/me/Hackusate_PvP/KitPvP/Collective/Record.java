package me.Hackusate_PvP.KitPvP.Collective;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Record implements Comparable<Record>{
    private String playerName;
    private int kills;
    private static List<Record> rankingList;

    public Record(String playerName){
        this.playerName = playerName;
        Record.addPlayer(this);
    }

    public Record(String playerName, int kills) {
        this.playerName = playerName;
        this.kills = kills;
        Record.addPlayer(this);
    }

    private String getName(){
        return playerName;
    }

    private int getKills(){
        return kills;
    }

    public static void addPlayer(Record playerRecord){
        if (rankingList == null) {
            rankingList = new ArrayList<Record>();
            rankingList.add(playerRecord);
        }
        else if (!rankingList.contains(playerRecord)) {
            rankingList.add(playerRecord);
        }
    }

    private static Record getRecord(String playerName){
        for (Record p : rankingList) {
            if (p.getName().equals(playerName)) {
                return p;
            }
        }
        return null;
    }

    public static void addKill(String playerName) {
        Record record = Record.getRecord(playerName);
        if (record != null) {
            record.addKill();
        }
        else {
            new Record(playerName, 1);
        }
    }

    private void addKill(){
        kills++;
    }

    public String toString(){
        return ("Rank: " + (rankingList.indexOf(this) + 1) + " Player: " + playerName + ", Kills: " + kills + ".");
    }

    public String getPlayerStats(String playerName){
        Record.sortRankings();
        return Record.getRecord(playerName).toString();
    }

    public static String[] getTopRankings(int top){
        Record.sortRankings();
        String[] rankings = new String[top];
        for (int i = 0; i < top; i++){
            rankings[i] = rankingList.get(i).toString();
        }
        return rankings;
    }

    public static String[] getTopRankings(){
        Record.sortRankings();
        int totalRecords = rankingList.size();
        String[] rankings = new String[totalRecords];
        for (int i = 0; i < totalRecords; i++){
            rankings[i] = rankingList.get(i).toString();
        }
        return rankings;
    }

    private static void sortRankings(){
        Collections.sort(rankingList);
    }

    public int compareTo(Record record){
        return Integer.compare(record.getKills(), this.getKills());
    }
}
