package me.Hackusate_PvP.KitPvP.Fixes;

import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import java.util.Iterator;
import java.util.List;

public class Patches implements Listener {
    private List<String> worlds = Main.instance.getConfig().getStringList("Fall-Damage.Worlds");

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onMobSpawn(EntitySpawnEvent event) {
        if (!(Main.config.getBoolean("Managers.Mob-Spawning"))) {
            if (!(event.getEntityType() == EntityType.DROPPED_ITEM)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onEntityDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player)e.getEntity();
            if (e.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
                Iterator var4 = this.worlds.iterator();
                while (var4.hasNext()) {
                    String w = (String) var4.next();
                    World world = Bukkit.getWorld(w);
                    if (p.getWorld().equals(world)) {
                        e.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void ondeathb(PlayerDeathEvent event) {
        Player player = event.getEntity();
        if (Main.config.getBoolean("Settings.Instant-Spawn")) {
            player.spigot().respawn();
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onWeedKill(BlockDamageEvent event) {
        Player player = event.getPlayer();
        if (!(player.hasPermission("core.build"))) {
            event.setCancelled(true);
        }
    }


    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
    public void onweedkills2(BlockFadeEvent event) {
            event.setCancelled(true);
    }


    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void weather(WeatherChangeEvent event) {
        if (!(Main.config.getBoolean("Managers.Weather-Change"))) {
            event.setCancelled(true);
        }
    }




}
