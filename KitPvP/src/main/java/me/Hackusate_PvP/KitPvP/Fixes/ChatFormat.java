package me.Hackusate_PvP.KitPvP.Fixes;

import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatFormat implements Listener {

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if (Main.config.getBoolean("Settings.Chat.Enabled")) {
            String format = Main.config.getString("Settings.Chat.Format");
            format = format.replace("%player%", player.getName());
            format = format.replace("%group%", Main.getPermissions().getPrimaryGroup(player));
            format = format.replace("%msg%", event.getMessage());
            format = format.replace("<" + player.getName() + ">", "");
            if (player.hasPermission("core.chat.color")) {
                event.setFormat(ChatColor.translateAlternateColorCodes('&', format));
            } else {
                String format2 = Main.config.getString("Settings.Chat.Format");
                format2 = format2.replace("%player%", player.getName());
                format2 = format2.replace("%group%", Main.getPermissions().getPrimaryGroup(player));
                format2 = format2.replace("%msg%", "");
                format2 = format2.replace("<" + player.getName() + ">", "");
                event.setFormat(ChatColor.translateAlternateColorCodes('&', format2) + event.getMessage());
            }
        }
    }
}
