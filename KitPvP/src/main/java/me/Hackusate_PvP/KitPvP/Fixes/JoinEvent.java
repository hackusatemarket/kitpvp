package me.Hackusate_PvP.KitPvP.Fixes;

import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.util.List;

public class JoinEvent implements Listener {
    private List<String> mesg = Main.instance.getConfig().getStringList("Join.Message");

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String joinm = Main.config.getString("Join.Join");
        joinm = joinm.replace("%player%", player.getName());
        if (!(player.hasPlayedBefore())) {
            player.sendMessage("Fist Join Message");
            if (!(Main.config.getBoolean("Managers.Join-Message"))) {
                event.setJoinMessage(null);
            } else {
                event.setJoinMessage(ChatColor.translateAlternateColorCodes('&', joinm));
            }
        } else {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
            String msg = mesg.toString();
            msg = msg.replace("[", "");
            msg = msg.replace("]", "");
            msg = msg.replace(",", "");
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
            if (!(Main.config.getBoolean("Managers.Join-Message"))) {
                event.setJoinMessage(null);
            } else {
                event.setJoinMessage(ChatColor.translateAlternateColorCodes('&', joinm));
            }
        }
    }


}
