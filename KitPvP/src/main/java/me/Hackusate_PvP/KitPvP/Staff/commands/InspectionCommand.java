package me.Hackusate_PvP.KitPvP.Staff.commands;

import me.Hackusate_PvP.KitPvP.Main;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class InspectionCommand implements CommandExecutor, TabCompleter {
    private final Main utilities = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arguments) {
        if (sender instanceof Player) {
            Player player = (Player)sender;
            if (player.hasPermission("core.command.inspect")) {
                if (arguments.length != 0 && arguments.length <= 1) {
                    Player target = Bukkit.getServer().getPlayerExact(arguments[0]);
                    if (target == null) {
                        player.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cPlayer named '" + arguments[0] + "' not found."));
                    } else if (target.equals(player)) {
                        player.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cYou can not inspect yourself."));
                    } else {
                        this.utilities.getStaffModeListener().openInspectionMenu(player, target);
                        player.sendMessage(ChatColor.translateAlternateColorCodes('&',Main.getAltColor() + "You are now inspecting the inventory of " + Main.getMainColor() + target.getName() + Main.getAltColor() + "."));
                    }
                } else {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cUsage: /" + label + " <playerName>"));
                }
            } else {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
            }
        } else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cYou can not execute this command on console."));
        }

        return false;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] arguments) {
        return arguments.length > 1 ? Collections.emptyList() : null;
    }
}
