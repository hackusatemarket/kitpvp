package me.Hackusate_PvP.KitPvP.Staff.events;

import me.Hackusate_PvP.KitPvP.Main;
import me.Hackusate_PvP.KitPvP.Utils.ColorUtils;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Utils.Staff;
import me.Hackusate_PvP.KitPvP.Utils.TimeUtils;
import org.apache.commons.lang.WordUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class StaffModeEvent implements Listener {
    private final Main utilities = Main.getInstance();
    private final Set<UUID> staffMode = new HashSet();
    private final Set<UUID> staffChat = new HashSet();
    private final Set<UUID> vanished = new HashSet();
    private final HashMap<UUID, UUID> inspectedPlayer = new HashMap();
    private final HashMap<UUID, ItemStack[]> contents = new HashMap();
    private final HashMap<UUID, ItemStack[]> armorContents = new HashMap();
    public Inventory inv;

    public StaffModeEvent() {
    }

    public Player getInspectedPlayer(Player player) {
        return Bukkit.getServer().getPlayer((UUID)this.inspectedPlayer.get(player.getUniqueId()));
    }

    public boolean isVanished(Player player) {
        return this.vanished.contains(player.getUniqueId());
    }


    public boolean isStaffChatActive(Player player) {
        return this.staffChat.contains(player.getUniqueId());
    }

    public boolean isStaffModeActive(Player player) {
        return this.staffMode.contains(player.getUniqueId());
    }

    public boolean hasPreviousInventory(Player player) {
        return this.contents.containsKey(player.getUniqueId()) && this.armorContents.containsKey(player.getUniqueId());
    }

    public void saveInventory(Player player) {
        this.contents.put(player.getUniqueId(), player.getInventory().getContents());
        this.armorContents.put(player.getUniqueId(), player.getInventory().getArmorContents());
    }

    public void loadInventory(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        playerInventory.setContents((ItemStack[])this.contents.get(player.getUniqueId()));
        playerInventory.setArmorContents((ItemStack[])this.armorContents.get(player.getUniqueId()));
        this.contents.remove(player.getUniqueId());
        this.armorContents.remove(player.getUniqueId());
    }

    public void setStaffChat(Player player, boolean status) {
        if (status) {
            if (player.hasPermission("core.staff")) {
                this.staffChat.add(player.getUniqueId());
            }
        } else {
            this.staffChat.remove(player.getUniqueId());
        }

    }

    public void setStaffMode(Player player, boolean status) {
        PlayerInventory playerInventory;
        if (status) {
            if (player.hasPermission("core.staff")) {
                this.staffMode.add(player.getUniqueId());
                this.saveInventory(player);
                playerInventory = player.getInventory();
                playerInventory.setArmorContents(new ItemStack[]{new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});
                playerInventory.clear();
                this.setItems(player);
                this.setVanished(player, true);
                player.updateInventory();
                if (player.getGameMode() == GameMode.SURVIVAL) {
                    player.setGameMode(GameMode.CREATIVE);
                }
            } else {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
            }
        } else {
            this.staffMode.remove(player.getUniqueId());
            playerInventory = player.getInventory();
            playerInventory.setArmorContents(new ItemStack[]{new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});
            playerInventory.clear();
            player.setGameMode(GameMode.SURVIVAL);
            this.setVanished(player, false);
            if (this.hasPreviousInventory(player)) {
                this.loadInventory(player);
            }

            player.updateInventory();
            if (!player.hasPermission("core.staff") && player.getGameMode() == GameMode.CREATIVE) {
                player.setGameMode(GameMode.SURVIVAL);
            }
        }

    }

    public void setVanished(Player player, boolean status) {
        Player online;
        Iterator var4;
        PlayerInventory playerInventory;
        if (status) {
            this.vanished.add(player.getUniqueId());
            var4 = Bukkit.getServer().getOnlinePlayers().iterator();

            while(var4.hasNext()) {
                online = (Player)var4.next();
                if (!online.hasPermission("core.staff")) {
                    online.hidePlayer(player);
                }
            }

            if (this.isStaffModeActive(player)) {
                playerInventory = player.getInventory();
                playerInventory.setItem(7, this.getVanishItemFor(player));
            }
        } else {
            this.vanished.remove(player.getUniqueId());
            var4 = Bukkit.getServer().getOnlinePlayers().iterator();

            while(var4.hasNext()) {
                online = (Player)var4.next();
                online.showPlayer(player);
            }

            if (this.isStaffModeActive(player)) {
                playerInventory = player.getInventory();
                playerInventory.setItem(7, this.getVanishItemFor(player));
            }
        }

    }

    public void setItems(Player player) {
        PlayerInventory playerInventory = player.getInventory();
        ItemStack compass = new ItemStack(Material.COMPASS, 1);
        ItemMeta compassMeta = compass.getItemMeta();
        compassMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Staff.Items.Compass.Name")));
        compassMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList(Main.config.getString("Staff.Items.Compass.Lore"))));
        compass.setItemMeta(compassMeta);
        ItemStack book = new ItemStack(Material.BOOK, 1);
        ItemMeta bookMeta = book.getItemMeta();
        bookMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Staff.Items.Book.Name")));
        bookMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList(Main.config.getString("Staff.Items.Book.Lore"))));
        book.setItemMeta(bookMeta);
        ItemStack blazeRod = new ItemStack(Material.ICE, 1);
        ItemMeta blazeRodMeta = blazeRod.getItemMeta();
        blazeRodMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Staff.Items.Ice.Name")));
        blazeRodMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList(Main.config.getString("Staff.Items.Ice.Lore"))));
        blazeRod.setItemMeta(blazeRodMeta);
        ItemStack carpet = new ItemStack(Material.WOOD_AXE);
        ItemMeta carpetMeta = carpet.getItemMeta();
        carpetMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Staff.Items.Wood_Axe.Name")));
        carpetMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList(Main.config.getString("Staff.Items.Wood_Axe.Lore"))));
        carpet.setItemMeta(carpetMeta);
        ItemStack record10 = new ItemStack(Material.RECORD_10, 1);
        ItemMeta record10Meta = record10.getItemMeta();
        record10Meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Staff.Items.Record.Name")));
        record10Meta.setLore((new ColorUtils()).translateFromArray(Arrays.asList(Main.config.getString("Staff.Items.Record.Lore"))));
        record10.setItemMeta(record10Meta);
        playerInventory.setItem(0, compass);
        playerInventory.setItem(1, book);
        playerInventory.setItem(4, blazeRod);
        playerInventory.setItem(2, carpet);
        playerInventory.setItem(7, this.getVanishItemFor(player));
        playerInventory.setItem(8, record10);
    }

    private ItemStack getVanishItemFor(Player player) {
        ItemStack inkSack = null;
        ItemMeta inkSackMeta;
        if (this.isVanished(player)) {
            inkSack = new ItemStack(Material.INK_SACK, 1, (short)8);
            inkSackMeta = inkSack.getItemMeta();
            inkSackMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Staff.Vanish.Enabled.Name")));
            inkSackMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList(Main.config.getString("Staff.Vanish.Enabled.Lore"))));
            inkSack.setItemMeta(inkSackMeta);
        } else {
            inkSack = new ItemStack(Material.INK_SACK, 1, (short)10);
            inkSackMeta = inkSack.getItemMeta();
            inkSackMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Staff.Vanish.Disabled.Name")));
            inkSackMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList(Main.config.getString("Staff.Vanish.Disabled.Lore"))));
            inkSack.setItemMeta(inkSackMeta);
        }

        return inkSack;
    }

    public void openInspectionMenu(final Player player, final Player target) {
        final Inventory inventory = Bukkit.getServer().createInventory((InventoryHolder)null, 54, (new ColorUtils()).translateFromString("&eInspecting: &c" + target.getName()));
        (new BukkitRunnable() {
            public void run() {
                StaffModeEvent.this.inspectedPlayer.put(player.getUniqueId(), target.getUniqueId());
                PlayerInventory playerInventory = target.getInventory();
                ItemStack speckledMelon = new ItemStack(Material.SPECKLED_MELON, (int)target.getHealth());
                ItemMeta speckledMelonMeta = speckledMelon.getItemMeta();
                speckledMelonMeta.setDisplayName((new ColorUtils()).translateFromString("&aHealth"));
                speckledMelon.setItemMeta(speckledMelonMeta);
                ItemStack cookedBeef = new ItemStack(Material.COOKED_BEEF, target.getFoodLevel());
                ItemMeta cookedBeefMeta = cookedBeef.getItemMeta();
                cookedBeefMeta.setDisplayName((new ColorUtils()).translateFromString("&aHunger"));
                cookedBeef.setItemMeta(cookedBeefMeta);
                ItemStack brewingStand = new ItemStack(Material.BREWING_STAND_ITEM, target.getPlayer().getActivePotionEffects().size());
                ItemMeta brewingStandMeta = brewingStand.getItemMeta();
                brewingStandMeta.setDisplayName((new ColorUtils()).translateFromString("&aActive Potion Effects"));
                ArrayList<String> brewingStandLore = new ArrayList();
                Iterator var10 = target.getPlayer().getActivePotionEffects().iterator();

                while(var10.hasNext()) {
                    PotionEffect potionEffect = (PotionEffect)var10.next();
                    String effectName = potionEffect.getType().getName();
                    int effectLevel = potionEffect.getAmplifier();
                    ++effectLevel;
                    brewingStandLore.add((new ColorUtils()).translateFromString("&e" + WordUtils.capitalizeFully(effectName).replace("_", " ") + " " + effectLevel + "&7: &c" + TimeUtils.IntegerCountdown.setFormat(potionEffect.getDuration() / 20)));
                }

                brewingStandMeta.setLore(brewingStandLore);
                brewingStand.setItemMeta(brewingStandMeta);
                ItemStack compass = new ItemStack(Material.COMPASS, 1);
                ItemMeta compassMeta = compass.getItemMeta();
                compassMeta.setDisplayName((new ColorUtils()).translateFromString("&aPlayer Location"));
                compassMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList("&eWorld&7: &a" + player.getWorld().getName(), "&eCoords", "  &eX&7: &c" + target.getLocation().getBlockX(), "  &eY&7: &c" + target.getLocation().getBlockY(), "  &eZ&7: &c" + target.getLocation().getBlockZ())));
                compass.setItemMeta(compassMeta);
                ItemStack ice = new ItemStack(Material.ICE, 1);
                ItemMeta iceMeta = ice.getItemMeta();
                if (StaffModeEvent.this.utilities.getFreezeListener().isFrozen(target)) {
                    iceMeta.setDisplayName((new ColorUtils()).translateFromString("&aFrozen&7: &aTrue"));
                } else {
                    iceMeta.setDisplayName((new ColorUtils()).translateFromString("&aFrozen&7: &cFalse"));
                }

                iceMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList("&eClick to update freeze status")));
                ice.setItemMeta(iceMeta);
                ItemStack orangeGlassPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)1);
                inventory.setContents(playerInventory.getContents());
                inventory.setItem(36, playerInventory.getHelmet());
                inventory.setItem(37, playerInventory.getChestplate());
                inventory.setItem(38, playerInventory.getLeggings());
                inventory.setItem(39, playerInventory.getBoots());
                inventory.setItem(40, playerInventory.getItemInHand());

                int i;
                for(i = 41; i <= 46; ++i) {
                    inventory.setItem(i, orangeGlassPane);
                }

                inventory.setItem(47, speckledMelon);
                inventory.setItem(48, cookedBeef);
                inventory.setItem(49, brewingStand);
                inventory.setItem(50, compass);
                inventory.setItem(51, ice);

                for(i = 52; i <= 53; ++i) {
                    inventory.setItem(i, orangeGlassPane);
                }

                if (!player.getOpenInventory().getTitle().equals((new ColorUtils()).translateFromString("&eInspecting: &c" + target.getName()))) {
                    this.cancel();
                    StaffModeEvent.this.inspectedPlayer.remove(player.getUniqueId());
                }

            }
        }).runTaskTimer(this.utilities, 0L, 5L);
        player.openInventory(inventory);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player)event.getWhoClicked();
        ItemStack clicked = event.getCurrentItem();
        Inventory inventory = event.getInventory();
        if (event.getClickedInventory() != null) {
            if (this.isStaffModeActive(player)) {
                event.setCancelled(true);
            }

            if (inventory.getName().equals("Xrayer Gui") && clicked.getType() == Material.SKULL_ITEM) {
                Bukkit.dispatchCommand(player, "tp " + clicked.getItemMeta().getDisplayName());
            }

            if (event.getInventory().getTitle().contains((new ColorUtils()).translateFromString("&eInspecting: "))) {
                Player inspected = this.getInspectedPlayer(player);
                if (event.getRawSlot() == 51 && inspected != null) {
                    if (this.utilities.getFreezeListener().isFrozen(inspected)) {
                        this.utilities.getFreezeListener().setFreeze(player, inspected, false);
                    } else if (!inspected.hasPermission("rank.staff") && !inspected.isOp()) {
                        this.utilities.getFreezeListener().setFreeze(player, inspected, true);
                    } else {
                        player.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.STAFF_FREEZE.toString()));
                    }
                }
            }
        }

    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        Action action = event.getAction();
        if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Player player = event.getPlayer();
            if (this.isStaffModeActive(player) && player.hasPermission("core.staff")) {
                ItemStack itemStack = player.getItemInHand();
                if (itemStack != null) {
                    if (itemStack.getType() == Material.DIAMOND_PICKAXE) {
                        int Counter = 0;
                        this.inv = Bukkit.createInventory((InventoryHolder)null, 54, "Xrayer Gui");
                        Iterator var7 = Bukkit.getServer().getOnlinePlayers().iterator();

                        while(var7.hasNext()) {
                            Player players = (Player)var7.next();
                            ++Counter;
                            if (Counter < 54) {
                                if (players.getLocation().getBlockY() < 20) {
                                    ItemStack xSkull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
                                    ItemMeta xSkullMeta = xSkull.getItemMeta();
                                    xSkullMeta.setDisplayName(players.getName());
                                    xSkullMeta.setLore((new ColorUtils()).translateFromArray(Arrays.asList(Main.getAltColor() + "This player is in the " + Main.getMainColor() + "&l" + players.getLocation().getBlockY() + Main.getAltColor() + " layer")));
                                    xSkull.setItemMeta(xSkullMeta);
                                    this.inv.addItem(new ItemStack[]{xSkull});
                                }
                            } else {
                                event.getPlayer().sendMessage((new ColorUtils()).translateFromString("&cThere are too many players mining right now."));
                            }
                        }

                        player.openInventory(this.inv);
                    }

                    if (itemStack.getType() == Material.RECORD_10) {
                        if (Bukkit.getServer().getOnlinePlayers().size() > 1) {
                            Random random = new Random();
                            int size = random.nextInt(Bukkit.getServer().getOnlinePlayers().size());
                            Player online = (Player)Bukkit.getServer().getOnlinePlayers().toArray()[size];
                            if (online.equals(player)) {
                                random.nextInt();
                                this.onPlayerInteract(event);
                                return;
                            }

                            event.setCancelled(true);
                            player.teleport(online);
                            player.sendMessage((new ColorUtils()).translateFromString(Main.getAltColor() + "eYou've been randomly teleported to " + Main.getMainColor() + online.getName() + Main.getAltColor() + "."));
                        } else {
                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.TELEPORT_FAILED.toString()));
                        }
                    } else if (itemStack.getType() == Material.INK_SACK) {
                        if (this.isVanished(player)) {
                            this.setVanished(player, false);
                        } else {
                            this.setVanished(player, true);
                        }
                    }
                }
            }
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
        if (event.getRightClicked() != null && event.getRightClicked() instanceof Player) {
            Player player = event.getPlayer();
            if (this.isStaffModeActive(player) && player.hasPermission("rank.staff")) {
                ItemStack itemStack = player.getItemInHand();
                if (itemStack != null) {
                    Player target = (Player)event.getRightClicked();
                    if (itemStack.getType() == Material.BOOK) {
                        if (target != null && !player.getName().equals(target.getName())) {
                            this.openInspectionMenu(player, target);
                            player.sendMessage((new ColorUtils()).translateFromString(Main.getAltColor() + "&You are now inspecting the inventory of " + Main.getMainColor() + target.getName() + Main.getAltColor() + "."));
                        }
                    } else if (itemStack.getType() == Material.CARROT_STICK) {
                        if (target != null && !player.getName().equals(target.getName())) {
                            if (player.getVehicle() != null) {
                                player.getVehicle().eject();
                            }

                            target.setPassenger(player);
                        }
                    } else if (itemStack.getType() == Material.ICE && target != null && !player.getName().equals(target.getName())) {
                        if (this.utilities.getFreezeListener().isFrozen(target)) {
                            this.utilities.getFreezeListener().setFreeze(player, target, false);
                        } else if (!target.hasPermission("core.staff") && !target.isOp()) {
                            this.utilities.getFreezeListener().setFreeze(player, target, true);
                        } else {
                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.STAFF_FREEZE.toString()));
                        }
                    }
                }
            }
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (!player.hasPermission("core.creative") && player.getGameMode() == GameMode.CREATIVE) {
            player.setGameMode(GameMode.CREATIVE);
        }

        Iterator var4;
        if (player.hasPermission("core.staff")) {
            String staffenter = Staff.ENTER_STAFF.toString();
            staffenter = staffenter.replace("%player%", player.getName());
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', staffenter));
            var4 = Bukkit.getServer().getOnlinePlayers().iterator();

            while(var4.hasNext()) {
                Player staff = (Player)var4.next();
                if (staff.hasPermission("core.staff")) {
                    String staffleave = Staff.LEAVE_STAFF.toString();
                    staff.sendMessage(ChatColor.translateAlternateColorCodes('&', staffenter));
                }
            }
        } else if (this.vanished.size() > 0) {
            var4 = this.vanished.iterator();

            while(var4.hasNext()) {
                UUID uuid = (UUID)var4.next();
                Player vanishedPlayer = Bukkit.getServer().getPlayer(uuid);
                if (vanishedPlayer != null) {
                    player.hidePlayer(vanishedPlayer);
                }
            }
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (player.hasPermission("rank.staff")) {
            String staffleave = Staff.LEAVE_STAFF.toString();
            staffleave = staffleave.replace("%player%", player.getName());
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', staffleave));
            Iterator var4 = Bukkit.getServer().getOnlinePlayers().iterator();

            while(var4.hasNext()) {
                Player staff = (Player)var4.next();
                if (staff.hasPermission("rank.staff")) {
                    staff.sendMessage(ChatColor.translateAlternateColorCodes('&', staffleave));
                }
            }
        }

        if (this.isStaffModeActive(player)) {
            this.staffMode.remove(player.getUniqueId());
            this.inspectedPlayer.remove(player.getUniqueId());
            PlayerInventory playerInventory = player.getInventory();
            playerInventory.setArmorContents(new ItemStack[]{new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR)});
            playerInventory.clear();
            this.setVanished(player, false);
            if (this.hasPreviousInventory(player)) {
                this.loadInventory(player);
            }

            if (!player.hasPermission("core.creative") && player.getGameMode() == GameMode.CREATIVE) {
                player.setGameMode(GameMode.SURVIVAL);
            }
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerDropItem(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        if (this.isStaffModeActive(player)) {
            event.setCancelled(true);
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerPickupItem(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();
        if (this.isVanished(player) || this.isStaffModeActive(player)) {
            event.setCancelled(true);
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        if ((this.isVanished(player) || this.isStaffModeActive(player)) && block != null) {
            event.setCancelled(true);
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        if ((this.isVanished(player) || this.isStaffModeActive(player)) && block != null) {
            event.setCancelled(true);
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Player player;
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            player = (Player)event.getDamager();
            if (this.isVanished(player) || this.isStaffModeActive(player) && player.hasPermission("core.staff")) {
                event.setCancelled(true);
            }
        } else if (event.getEntity() instanceof LivingEntity && event.getDamager() instanceof Player) {
            player = (Player)event.getDamager();
            if (this.isVanished(player) || this.isStaffModeActive(player) && player.hasPermission("core.staff")) {
                event.setCancelled(true);
            }
        }

    }
}


