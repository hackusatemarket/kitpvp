package me.Hackusate_PvP.KitPvP.Staff.commands;

import me.Hackusate_PvP.KitPvP.Utils.ColorUtils;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Utils.Staff;
import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class FreezeCommand implements CommandExecutor, TabCompleter {
    private final Main utilities = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arguments) {
        if (sender.hasPermission("core.command.freeze")) {
            if (arguments.length != 0 && arguments.length <= 1) {
                Player target = Bukkit.getServer().getPlayerExact(arguments[0]);
                if (target == null) {
                    sender.sendMessage((new ColorUtils()).translateFromString("&cPlayer named '" + arguments[0] + "' not found."));
                } else if (target.equals(sender)) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.STAFF_FREEZE_SELF.toString()));
                } else if (sender instanceof Player) {
                    if (!target.hasPermission("core.bypass.freeze") && !target.isOp()) {
                        if (this.utilities.getFreezeListener().isFrozen(target)) {
                            this.utilities.getFreezeListener().setFreeze(sender, target, false);
                        } else {
                            String msgmsg = Main.config.getString("Executor.Command.Msg");
                            msgmsg = msgmsg.replace("%target%", arguments[0]);
                            Bukkit.dispatchCommand(sender, ChatColor.translateAlternateColorCodes('&', msgmsg));
                            this.utilities.getFreezeListener().setFreeze(sender, target, true);
                        }
                    } else {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.STAFF_FREEZE.toString()));
                    }
                } else if (this.utilities.getFreezeListener().isFrozen(target)) {
                    this.utilities.getFreezeListener().setFreeze(sender, target, false);
                } else {
                    String msgmsg = Main.config.getString("Executor.Command.Msg");
                    msgmsg = msgmsg.replace("%target%", arguments[0]);
                    Bukkit.dispatchCommand(sender, ChatColor.translateAlternateColorCodes('&', msgmsg));
                    this.utilities.getFreezeListener().setFreeze(sender, target, true);
                }
            } else {
                sender.sendMessage((new ColorUtils()).translateFromString("&cUsage: /" + label + " <playerName>"));
            }
        } else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }
        return false;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] arguments) {
        return arguments.length > 1 ? Collections.emptyList() : null;
    }
}

