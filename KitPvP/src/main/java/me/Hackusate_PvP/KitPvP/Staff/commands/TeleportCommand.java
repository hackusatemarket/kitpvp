package me.Hackusate_PvP.KitPvP.Staff.commands;

import me.Hackusate_PvP.KitPvP.Main;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Utils.Staff;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class TeleportCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {
        if (p.hasPermission("core.command.teleport")) {

            if (args.length == 0) {
                p.sendMessage(ChatColor.RED + "Usage: /tp <player> <player2 | x,y,z>");
            } if (args.length == 1) {
                Player player = (Player) p;
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target != null) {
                    Location playerloc = player.getLocation();
                    Location targetloc = target.getLocation();
                    player.teleport(targetloc);
                    String conmsg = Staff.TELEPORT_PLAYER_ALERT.toString();
                    conmsg = conmsg.replace("%sender%", p.getName());
                    conmsg = conmsg.replace("%target%", target.getName());
                    Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', conmsg));
                    String msg = Staff.TELEPORT_TO_PLAYER.toString();
                    msg = msg.replace("%target%", target.getName());
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
                } else {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.PLAYER_NOT_FOUND.toString()));
                }
            }

            if (args.length == 2) {
                Player target = Bukkit.getPlayerExact(args[0]);
                Player target2 = Bukkit.getPlayerExact(args[1]);
                if (target != null && target2 != null) {
                    Location targetloc = target.getLocation();
                    Location targetloc2 = target2.getLocation();
                    target.teleport(targetloc2);
                    String conmsg = Staff.TELEPORT_PLAYER_TO_PLAYER_ALERT.toString();
                    conmsg = conmsg.replace("%sender%", p.getName());
                    conmsg = conmsg.replace("%target%", target.getName());
                    conmsg = conmsg.replace("%target2%", target2.getName());
                    Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', conmsg));
                    String msg = Staff.TELEPORT_PLAYER_TO_PLAYER.toString();
                    msg = msg.replace("%sender%", p.getName());
                    msg = msg.replace("%target%", target2.getName());
                    target.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
                } else {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.PLAYER_NOT_FOUND.toString()));
                }
            }

            if (args.length == 4) {
                p.sendMessage(ChatColor.RED + "Coordinate teleporting is not done yet.");
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target != null) {
                    target.teleport(new Location(Bukkit.getWorld("world"), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3])));
                    String conmsg = Staff.TELEPORT_PLAYER_TO_COORDS_ALERT.toString();
                    conmsg = conmsg.replace("%sender%", p.getName());
                    conmsg = conmsg.replace("%target%", target.getName());
                    conmsg = conmsg.replace("%x%", args[1]);
                    conmsg = conmsg.replace("%y%", args[2]);
                    conmsg = conmsg.replace("%z%", args[3]);
                    Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', conmsg));
                    String msg = Staff.TELEPORT_PLAYER_TO_COORDS.toString();
                    msg = msg.replace("%target%", target.getName());
                    msg = msg.replace("%x%", args[1]);
                    msg = msg.replace("%y%", args[2]);
                    msg = msg.replace("%z%", args[3]);
                    String targetmsg = Staff.TELEPORT_PLAYER_TO_COORDS_TARGET.toString();
                    targetmsg = targetmsg.replace("%sender%", p.getName());
                    targetmsg = targetmsg.replace("%target%", target.getName());
                    targetmsg = targetmsg.replace("%x%", args[1]);
                    targetmsg = targetmsg.replace("%y%", args[2]);
                    targetmsg = targetmsg.replace("%z%", args[3]);
                    target.sendMessage(ChatColor.translateAlternateColorCodes('&', targetmsg));
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
                } else {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.PLAYER_NOT_FOUND.toString()));
                }
            }

            if (args.length == 3) {
                p.sendMessage(ChatColor.RED + "Coordinate teleporting is not done yet.");
                String conmsg = Staff.TELEPORT_SENDER_TO_COORDS_ALERT.toString();
                conmsg = conmsg.replace("%sender%", p.getName());
                conmsg = conmsg.replace("%x%", args[0]);
                conmsg = conmsg.replace("%y%", args[1]);
                conmsg = conmsg.replace("%z%", args[2]);
                String msg = Staff.TELEPORT_SENDER_TO_COORDS.toString();
                msg = msg.replace("%sender%", p.getName());
                msg = msg.replace("%x%", args[0]);
                msg = msg.replace("%y%", args[1]);
                msg = msg.replace("%z%", args[2]);
                Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', conmsg));
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
                ((Player) p).teleport(new Location(Bukkit.getWorld("world"), Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2])));
            }

        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }



        return false;
    }
}
