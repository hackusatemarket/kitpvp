package me.Hackusate_PvP.KitPvP.Staff.events;

import me.Hackusate_PvP.KitPvP.Main;
import me.Hackusate_PvP.KitPvP.Utils.FancyMessage;
import me.Hackusate_PvP.KitPvP.Utils.Staff;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.*;
import org.bukkit.potion.PotionEffect;

import java.util.*;

public class FreezeEvent implements Listener {
    private final Main utilities = Main.getInstance();
    private final Set<UUID> frozen = new HashSet();
    private FreezeEvent.AlertTask alertTask;
    private List<String> mesg = Main.instance.getConfig().getStringList("Staff.Messages.Freeze.Freeze-Player-Message");

    public FreezeEvent() {
    }

    public boolean isFrozen(Player player) {
        return this.frozen.contains(player.getUniqueId());
    }

    public void setFreeze(CommandSender sender, Player target, boolean status) {
        Player staff;
        Iterator var5;
        if (status) {
            this.frozen.add(target.getUniqueId());
            this.alertTask = new AlertTask(target);
            if (sender instanceof Player) {
                String frma = Staff.STAFF_ALERT.toString();
                frma = frma.replace("%target%", target.getName());
                frma = frma.replace("%player%", sender.getName());
                Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', frma));
                var5 = Bukkit.getServer().getOnlinePlayers().iterator();

                while(var5.hasNext()) {
                    staff = (Player)var5.next();
                    if (staff.hasPermission("core.staff")) {
                        if (staff.equals(sender)) {
                            String fpm = Staff.STAFF_ALERT_FREEZE_MESSAGE.toString();
                            fpm = fpm.replace("%player%", sender.getName());
                            fpm = fpm.replace("%target%", target.getName());
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', fpm));
                        } else {
                            String frzpm = Staff.STAFF_ALERT_FREEZE_MESSAGE.toString();
                            frzpm = frzpm.replace("%player%", sender.getName());
                            frzpm = frzpm.replace("%target%", target.getName());
                            staff.sendMessage(ChatColor.translateAlternateColorCodes('&', frzpm));
                        }
                    }
                }
            } else {
                String froze = Staff.STAFF_FROZE_MESSAGE.toString();
                froze = froze.replace("%target%", target.getName());
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', froze));
                var5 = Bukkit.getServer().getOnlinePlayers().iterator();

                while(var5.hasNext()) {
                    staff = (Player)var5.next();
                    if (staff.hasPermission("core.staff")) {
                        String setfreeze = Staff.STAFF_ALERT_SET_FROZE_MESSAGE.toString();
                        setfreeze = setfreeze.replace("%target%", target.getName());
                        setfreeze = setfreeze.replace("%player%", sender.getName());
                        staff.sendMessage(ChatColor.translateAlternateColorCodes('&', setfreeze));
                    }
                }
            }
        } else {
            this.frozen.remove(target.getUniqueId());
            target.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.STAFF_UNFREEZE_PLAYER.toString()));
            if (sender instanceof Player) {
                String freezestaff = Staff.STAFF_ALERT_UNFREEZE_PLAYER.toString();
                freezestaff = freezestaff.replace("%player%", sender.getName());
                freezestaff = freezestaff.replace("%target%", target.getName());
                Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&', freezestaff));
                var5 = Bukkit.getServer().getOnlinePlayers().iterator();

                while(var5.hasNext()) {
                    staff = (Player)var5.next();
                    if (staff.hasPermission("core.staff")) {
                        if (staff.equals(sender)) {
                            String frozen2 = Staff.STAFF_UNFROZEN_STAFF_MESSAGE.toString();
                            frozen2 = frozen2.replace("%target%", target.getName());
                            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', frozen2));
                        } else {
                            staff.sendMessage(ChatColor.translateAlternateColorCodes('&', freezestaff));
                        }
                    }
                }
            } else {
                String frozen2 = Staff.STAFF_UNFROZEN_STAFF_MESSAGE.toString();
                frozen2 = frozen2.replace("%target%", target.getName());
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', frozen2));
                var5 = Bukkit.getServer().getOnlinePlayers().iterator();

                while(var5.hasNext()) {
                    staff = (Player)var5.next();
                    if (staff.hasPermission("core.staff")) {
                        String freezestaff = Staff.STAFF_ALERT_UNFREEZE_PLAYER.toString();
                        freezestaff = freezestaff.replace("%player%", sender.getName());
                        freezestaff = freezestaff.replace("%target%", target.getName());
                        staff.sendMessage(ChatColor.translateAlternateColorCodes('&', freezestaff));
                    }
                }
            }
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        Location from = event.getFrom();
        Location to = event.getTo();
        if (this.isFrozen(player) && (from.getBlockX() != to.getBlockX() || from.getBlockZ() != to.getBlockZ())) {
            event.setTo(from);
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        if (event.getEntity().getShooter() instanceof Player) {
            Player player = (Player)event.getEntity().getShooter();
            if (this.isFrozen(player)) {
                event.setCancelled(true);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou can not interact with that item while you are frozen."));
            }
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
        Player player = event.getPlayer();
        if (this.isFrozen(player) && !event.getMessage().startsWith("/helpop") && !event.getMessage().startsWith("/faction chat") && !event.getMessage().startsWith("/fac chat") && !event.getMessage().startsWith("/f chat") && !event.getMessage().startsWith("/faction c") && !event.getMessage().startsWith("/fac c") && !event.getMessage().startsWith("/f c") && !event.getMessage().startsWith("/helpop") && !event.getMessage().startsWith("/request") && !event.getMessage().startsWith("/msg") && !event.getMessage().startsWith("/message") && !event.getMessage().startsWith("/reply")) {
            event.setCancelled(false);
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou can not use commands while you are frozen."));
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if (event.getBlock() != null && this.isFrozen(player)) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou can not place blocks while you are frozen."));
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if (event.getBlock() != null && this.isFrozen(player)) {
            event.setCancelled(true);
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou can not break blocks while you are frozen."));
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerKick(PlayerKickEvent event) {
        Player player = event.getPlayer();
        if (this.isFrozen(player)) {
            this.frozen.remove(player.getUniqueId());
        }

    }

    public Player getDamager(Entity entity) {
        if (entity instanceof Player) {
            return (Player)entity;
        } else {
            if (entity instanceof Projectile) {
                Projectile projectile = (Projectile)entity;
                if (projectile.getShooter() != null && projectile.getShooter() instanceof Player) {
                    return (Player)projectile.getShooter();
                }
            }

            return null;
        }
    }

    @EventHandler(
            ignoreCancelled = true,
            priority = EventPriority.HIGH
    )
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Player damager = this.getDamager(event.getDamager());
        Player damaged = this.getDamager(event.getEntity());
        if (damager != null && damaged != null && damaged != damager) {
            if (this.utilities.getFreezeListener().isFrozen(damager)) {
                damager.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou can not attack players while frozen."));
                event.setCancelled(true);
            }

            if (this.utilities.getFreezeListener().isFrozen(damaged)) {
                damager.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou can not attack " + damaged.getName() + " because he is currently frozen."));
                event.setCancelled(true);
            }
        }

    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if (this.isFrozen(player)) {
            Iterator var4 = Bukkit.getServer().getOnlinePlayers().iterator();

            while(true) {
                Player staff;
                do {
                    if (!var4.hasNext()) {
                        return;
                    }

                    staff = (Player)var4.next();
                } while(!staff.hasPermission("core.staff") && !staff.isOp());

                FancyMessage fancyMessage = new FancyMessage("");
                fancyMessage.then(ChatColor.translateAlternateColorCodes('&',Main.getAltColor() + "[Staff] " + Main.getMainColor() + player.getName() + Main.getOther() + " has disconnected while frozen. "));
                fancyMessage.then(ChatColor.translateAlternateColorCodes('&',Main.getAltColor() + "(Click here to ban)"));
                fancyMessage.tooltip(ChatColor.translateAlternateColorCodes('&',"&aClick to permanently ban " + player.getName()));
                fancyMessage.command("/ban " + player.getName() + " Refusal to Screensharing.");
                fancyMessage.send(staff);
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (this.isFrozen(player)) {
            this.alertTask = new AlertTask(player);
            Iterator var4 = Bukkit.getServer().getOnlinePlayers().iterator();

            while(var4.hasNext()) {
                Player staff = (Player)var4.next();
                if (staff.hasPermission("core.staff")) {
                    FancyMessage fancyMessage = new FancyMessage("");
                    fancyMessage.then(ChatColor.translateAlternateColorCodes('&',"&7[Staff] &6" + player.getName() + " &chas joined but he is frozen. "));
                    fancyMessage.then(ChatColor.translateAlternateColorCodes('&',"&7(Click here to ban)"));
                    fancyMessage.tooltip(ChatColor.translateAlternateColorCodes('&',"&aClick to permanently ban " + player.getName()));
                    fancyMessage.command("/ban " + player.getName() + " Refusal to Screensharing.");
                    fancyMessage.send(staff);
                }
            }
        }

    }

    private class AlertTask  {
        private  Player player;

        public AlertTask(Player player) {
            this.player = player;
            if (FreezeEvent.this.isFrozen(this.player)) {
                this.player.setHealth(this.player.getMaxHealth());
                this.player.setFireTicks(0);
                this.player.setFoodLevel(20);
                this.player.setSaturation(3.0F);
                Iterator var2 = this.player.getActivePotionEffects().iterator();

                while(var2.hasNext()) {
                    PotionEffect potionEffect = (PotionEffect)var2.next();
                    this.player.removePotionEffect(potionEffect.getType());
                }
                String msg = mesg.toString();
                msg = msg.replace("[", "");
                msg = msg.replace("]", "");
                msg = msg.replace(",", "");
               this.player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
            }

        }
    }
}

