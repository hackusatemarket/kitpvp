package me.Hackusate_PvP.KitPvP.Staff.commands;

import me.Hackusate_PvP.KitPvP.Main;
import me.Hackusate_PvP.KitPvP.Utils.ColorUtils;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Utils.Staff;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class VanishCommand implements CommandExecutor {
    private final Main utilities = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arguments) {
        if (sender instanceof Player) {
            Player player = (Player)sender;
            if (player.hasPermission("core.command.vanish")) {
                if (arguments.length > 0) {
                    player.sendMessage((new ColorUtils()).translateFromString("&cUsage: /" + label));
                } else if (this.utilities.getStaffModeListener().isVanished(player)) {
                    this.utilities.getStaffModeListener().setVanished(player, false);
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.VANISH_DISABLED_MESSAGE.toString()));
                } else {
                    this.utilities.getStaffModeListener().setVanished(player, true);
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.VANISH_ENABLED_MESSAGE.toString()));
                }
            } else {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
            }
        } else {
            sender.sendMessage((new ColorUtils()).translateFromString("&cYou can not execute this command on console."));
        }

        return false;
    }

}


