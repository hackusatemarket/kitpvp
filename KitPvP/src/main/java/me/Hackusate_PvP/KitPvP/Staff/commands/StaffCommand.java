package me.Hackusate_PvP.KitPvP.Staff.commands;

import me.Hackusate_PvP.KitPvP.Main;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Utils.Staff;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class StaffCommand implements CommandExecutor, TabCompleter {
    private final Main utilities = Main.getInstance();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] arguments) {
        if (sender instanceof Player) {
            Player player = (Player)sender;
            if (player.hasPermission("core.command.staffmode")) {
                if (arguments.length > 0) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cUsage: /" + label));
                } else if (this.utilities.getStaffModeListener().isStaffModeActive(player)) {
                    this.utilities.getStaffModeListener().setStaffMode(player, false);
                    if (Main.config.getBoolean("Settings.Staff-Teleport")) {
                        player.teleport(new Location(Bukkit.getWorld(Main.config.getString("Spawn.World")), Main.config.getInt("Spawn.X"), Main.config.getInt("Spawn.Y"), Main.config.getInt("Spawn.Z")));
                    }
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.STAFF_MODE_LEAVE.toString()));
                } else {
                    this.utilities.getStaffModeListener().setStaffMode(player, true);
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', Staff.STAFF_MODE_ENTER.toString()));
                }
            } else {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
            }
        } else {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cYou can not execute this command on console."));
        }

        return false;
    }

    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] arguments) {
        return arguments.length > 1 ? Collections.emptyList() : null;
    }
}

