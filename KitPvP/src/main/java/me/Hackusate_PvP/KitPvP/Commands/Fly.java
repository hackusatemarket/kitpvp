package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.UUID;

import static me.Hackusate_PvP.KitPvP.CoreAPI.fly;

public class Fly implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {

        if (p.hasPermission("core.command.fly")) {
            Player player = (Player) p;
            UUID uuid = player.getUniqueId();
            if (!(fly.contains(uuid))) {
                player.setAllowFlight(true);
                fly.add(uuid);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.FLY_ENABLED.toString()));
            } else {
                player.setAllowFlight(false);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.FLY_DSIABLED.toString()));
                fly.remove(uuid);
            }
        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }


        return false;
    }
}
