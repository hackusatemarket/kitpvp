package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class EChest implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {

        if (p.hasPermission("core.command.echest")) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.ECHEST_OPENED.toString()));
            ((Player) p).openInventory(((Player) p).getEnderChest());
        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }

        return false;
    }
}
