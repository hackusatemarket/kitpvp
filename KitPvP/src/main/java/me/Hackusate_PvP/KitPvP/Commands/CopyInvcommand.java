package me.Hackusate_PvP.KitPvP.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CopyInvcommand implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (sender.hasPermission("core.commands.copyinv")) {
                Player player = (Player) sender;
                if (args.length == 0) {
                    sender.sendMessage(ChatColor.RED + "Usage: /copyinv <player>");
                } else {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target != null) {
                        player.getInventory().setContents(target.getInventory().getContents());
                        sender.sendMessage(ChatColor.RED + "You have copied " + args[0] + "'s inventory");
                    } else {
                        player.sendMessage(ChatColor.RED + "Player not found.");
                    }
                }
            }
        }
        return false;
    }
}
