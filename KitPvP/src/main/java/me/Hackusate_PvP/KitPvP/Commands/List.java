package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class List implements CommandExecutor {

    public boolean onCommand(CommandSender p, org.bukkit.command.Command command, String label, String[] args) {

        if (args.length == 0) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getAltColor() + "There are " + Main.getMainColor()
                    + ChatColor.BOLD + Bukkit.getServer().getOnlinePlayers().size() + ChatColor.RESET + Main.getAltColor() + " players connected to " + ChatColor.BOLD + Main.getOther() + Main.config.getString("Core.Name")));
        }


        return false;

    }
}
