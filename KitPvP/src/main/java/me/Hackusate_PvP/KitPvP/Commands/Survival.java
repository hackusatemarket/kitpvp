package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Survival implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {

        if (p.hasPermission("core.command.survival")) {
            if (args.length == 0) {
                String msg = Lang.GAMEMODE_MESSAGE.toString();
                msg = msg.replace("%gm%", "SURVIVAL");
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
                ((Player) p).setGameMode(GameMode.SURVIVAL);
            }

            if (args.length == 1) {
                Player target = Bukkit.getPlayerExact(args[0]);
                if (target != null) {
                    target.setGameMode(GameMode.SURVIVAL);
                    String msg = Lang.GAMEMODE_MESSAGE_SENDER.toString();
                    msg = msg.replace("%gm%", "SURVIVAL");
                    msg = msg.replace("%target%", target.getName());
                    String targmsg = Lang.GAMEMODE_MESSAGE_TARGET.toString();
                    targmsg = targmsg.replace("%gm%", "CREATIVE");
                    targmsg = targmsg.replace("%sender%", p.getName());
                    targmsg = targmsg.replace("%target%", target.getName());
                    target.sendMessage(ChatColor.translateAlternateColorCodes('&', targmsg));
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
                } else {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.PLAYER_NOT_FOUND.toString()));
                }
            }

        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }

        return false;
    }
}
