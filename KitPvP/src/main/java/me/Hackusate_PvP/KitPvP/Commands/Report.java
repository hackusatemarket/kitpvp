package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Utils.Message;
import me.Hackusate_PvP.KitPvP.Utils.Staff;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Report implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {
        if (p.hasPermission("core.command.report")) {

            if (args.length == 0) {
                p.sendMessage(ChatColor.RED + "Usage: /report <player> reason");
            }

            else {

                if (args.length == 1) {
                    Player target = Bukkit.getServer().getPlayerExact(args[0]);
                    if (target != null) {
                        p.sendMessage(ChatColor.RED + "Usage: /report " + target.getName() + " <reason>");
                    } else {
                        p.sendMessage(ChatColor.RED + "Usage: /report " + args[0] + " <reason>");
                    }
                } else {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target != null) {
                        if (!(p.getName().equalsIgnoreCase(target.getName()))) {
                            String reason = Message.toString(args, 1);
                            p.sendMessage(ChatColor.GREEN + "Success! Your report has been sent out.");
                            String msg = Staff.REPORT_STAFF_MESSAGE.toString();
                            msg = msg.replace("%sender%", p.getName());
                            msg = msg.replace("%target%", target.getName());
                            msg = msg.replace("%reason%", reason);
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                if (player.hasPermission("core.staff")) {
                                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
                                }
                            }
                        } else {
                            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.REPORT_SELF.toString()));
                        }
                    } else {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.PLAYER_NOT_FOUND.toString()));
                    }
                }
            }

            } else {
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
            }

        return false;
    }
}
