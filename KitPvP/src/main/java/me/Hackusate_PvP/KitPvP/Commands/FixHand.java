package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Main;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FixHand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {
        if (p.hasPermission("core.command.fixhand")) {
            EconomyResponse r = Main.getEconomy().withdrawPlayer((Player) p, Main.config.getInt("Settings.FixHand.Price"));
            if (((Player) p).getInventory().getItemInHand() != null) {
                if (r.transactionSuccess()) {
                    ((Player) p).getInventory().getItemInHand().setDurability((short) 0);
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Settings.FixHand.Message")));
                    if (Main.config.getBoolean("Settings.Economy.Send-Message")) {
                        String ecom = Main.config.getString("Settings.Economy.Message");
                        ecom = ecom.replace("%price%", String.valueOf(Main.config.getInt("Settings.FixHand.Price")));
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', ecom));
                    }

                }
            } else {
                ((Player) p).sendMessage(ChatColor.RED + "Item not found.");
            }
        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }

        return false;
    }
}
