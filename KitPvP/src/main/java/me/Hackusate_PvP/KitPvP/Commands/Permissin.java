package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Permissin implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {

        if (p.hasPermission("core.command.permission")) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getMainColor() + "Permission" + Main.getAltColor() + " List"));
            p.sendMessage("");
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.core " + Main.getAltColor() + "("
            + "&oGives access to /core&r" + Main.getAltColor() + ")"));

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.creative " + Main.getAltColor() + "("
                    + "&oGives access to /creative&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.echest " + Main.getAltColor() + "("
                    + "&oGives access to /echest&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.feed " + Main.getAltColor() + "("
                    + "&oGives access to /feed&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.fixhand " + Main.getAltColor() + "("
                    + "&oGives access to /fixhand&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.permission " + Main.getAltColor() + "("
                    + "&oGives access to /permission&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.report " + Main.getAltColor() + "("
                    + "&oGives access to /report&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.survival " + Main.getAltColor() + "("
                    + "&oGives access to /survival&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.toggleblocks " + Main.getAltColor() + "("
                    + "&oGives access to /toggleblocks&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.clearchat " + Main.getAltColor() + "("
                    + "&oGives access to /clearchat&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.freeze " + Main.getAltColor() + "("
                    + "&oGives access to /freeze&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.inspect " + Main.getAltColor() + "("
                    + "&oGives access to /inspect&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.staff " + Main.getAltColor() + "("
                    + "&oGives access to /staff&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.teleport " + Main.getAltColor() + "("
                    + "&oGives access to /teleport&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.command.vanish " + Main.getAltColor() + "("
                    + "&oGives access to /vanish&r" + Main.getAltColor() + ")"));

            p.sendMessage("");

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "core.staff " + Main.getAltColor() + "("
                    + "&oGives access to to all staff tools&r" + Main.getAltColor() + ")"));

            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('*', Lang.NO_PERMS.toString()));
        }

        return false;
    }
}
