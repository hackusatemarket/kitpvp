package me.Hackusate_PvP.KitPvP.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class Changelog implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {
        try {
            String web = "https://bitbucket.org/hackusatemarket/kitpvp/raw/d56eecae9b3fc5c3971dff939861d1b2272fa39a/Changelog.txt";
            URL url = new URL(web);
            ArrayList<Object> lines = new ArrayList<>();
            URLConnection connection = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                line = line + "                ";
                lines.add(line);
            }
            if (line != null) {
                lines.add(line);
            }

            String message = lines.toString();
            message = message.replace("<", "");
            message = message.replace(">", "");
            message = message.replace("=", "");
            message = message.replace("null", "");
            message = message.replace("\"\"", "");
            message = message.replace("##", ChatColor.BOLD.toString());
            if (message.startsWith("[")) {
                message = message.replace("[", "");
            }
            if (message.endsWith("]")) {
                message = message.replace("]", "");
            }





            p.sendMessage(message);

        } catch (Exception var6) {
            p.sendMessage(ChatColor.RED + "Log not found!");
            Bukkit.getServer().shutdown();
        }
        return false;
    }
}
