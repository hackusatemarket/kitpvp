package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Feed implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {
        if (p.hasPermission("core.command.feed")) {
            if (args.length == 0) {
                Player player = (Player) p;
                player.setFoodLevel(50);
                player.setSaturation((float) 5.0);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.FEED_PLAYER.toString()));
            }
        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }

            if (args.length > 1) {
                if (p.hasPermission("core.command.feed.others")) {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target != null) {
                        target.setFoodLevel(30);
                        target.setSaturation((float) 3.0);
                        String msg = Lang.FEED_OTHER.toString();
                        msg = msg.replace("%player%", p.getName());
                        msg = msg.replace("%target%", target.getName());
                        String targ = Lang.FEED_TARGET.toString();
                        targ = targ.replace("%player%", p.getName());
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
                        target.sendMessage(ChatColor.translateAlternateColorCodes('&', targ));
                    } else {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.PLAYER_NOT_FOUND.toString()));
                    }
                } else {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
                }
            }


        return false;
    }
}
