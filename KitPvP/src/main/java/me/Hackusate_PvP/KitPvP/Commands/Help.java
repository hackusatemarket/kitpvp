package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Help implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {

        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getMainColor() + "Help " + Main.getAltColor() + "Page " + Main.getOther() + "(1/2)"));
        p.sendMessage("");
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + "* " + Main.getMainColor() + "/help " + Main.getAltColor() + "(" + "&oDisplays help page" + Main.getAltColor() + ")"));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + "* " + Main.getMainColor() + "/echest " + Main.getAltColor() + "(" + "&oOpens your echest" + Main.getAltColor() + ")"));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + "* " + Main.getMainColor() + "/feed " + Main.getAltColor() + "(" + "&oFill your apatite" + Main.getAltColor() + ")"));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + "* " + Main.getMainColor() + "/ffa " + Main.getAltColor() + "(" + "&oDisplays FFA information" + Main.getAltColor() + ")"));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + "* " + Main.getMainColor() + "/fixhand " + Main.getAltColor() + "(" + "&oFixes item in hand" + Main.getAltColor() + ")"));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + "* " + Main.getMainColor() + "/event " + Main.getAltColor() + "(" + "&oDisplays all events" + Main.getAltColor() + ")"));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + "* " + Main.getMainColor() + "/report " + Main.getAltColor() + "(" + "&oReport a player for misdeeds" + Main.getAltColor() + ")"));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + "* " + Main.getMainColor() + "/list " + Main.getAltColor() + "(" + "&oDisplays current players online" + Main.getAltColor() + ")"));
        p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));

        return false;
    }
}
