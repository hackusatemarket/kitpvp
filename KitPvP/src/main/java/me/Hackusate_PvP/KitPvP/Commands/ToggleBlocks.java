package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Utils.Lang;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandException;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ToggleBlocks implements CommandExecutor, Listener {
    private static final Set<UUID> toggled = new HashSet<UUID>();


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {

        if (p.hasPermission("core.command.toggleblocks")) {
            Player player = (Player) p;
            UUID uuid = player.getUniqueId();
            if (!(toggled.contains(uuid))) {
                toggled.add(uuid);
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.TOGGLE_BLOCK_DISABLED.toString()));
            } else {
                toggled.remove(uuid);
                p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.TOGGLE_BLOCK_ENABLED.toString()));
            }

        } else {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
        }

        return false;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        if (player.hasPermission("core.staff")) {
            if (toggled.contains(uuid)) {
                event.setCancelled(true);
            }
        } else {
            event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        if (player.hasPermission("core.staff")) {
            if (toggled.contains(uuid)) {
                event.setCancelled(true);
            }
        } else {
            event.setCancelled(true);
        }
    }
}
