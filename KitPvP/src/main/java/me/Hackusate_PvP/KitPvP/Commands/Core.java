package me.Hackusate_PvP.KitPvP.Commands;

import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Core implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {
        if (p.hasPermission("core.command.core")) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
            p.sendMessage(ChatColor.GOLD + "Developer: " + ChatColor.RED + "Hackusate_PvP");
            p.sendMessage(ChatColor.RED + "Version: " + ChatColor.RED + Main.plugin.getDescription().getVersion());
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
        } else {
            p.sendMessage(ChatColor.GRAY + ChatColor.STRIKETHROUGH.toString() + Main.getBar());
            p.sendMessage(ChatColor.GOLD + "Developer: " + ChatColor.RED + "Hackusate_PvP");
            p.sendMessage(ChatColor.RED + "Version: " + ChatColor.RED + Main.plugin.getDescription().getVersion());
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
        }

        return false;
    }
}
