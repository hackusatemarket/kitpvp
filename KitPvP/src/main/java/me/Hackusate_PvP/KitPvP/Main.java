package me.Hackusate_PvP.KitPvP;

import com.bizarrealex.aether.Aether;
import me.Hackusate_PvP.KitPvP.Board.BoardLink;
import me.Hackusate_PvP.KitPvP.Commands.*;
import me.Hackusate_PvP.KitPvP.Events.FFA.Commands.FFA;
import me.Hackusate_PvP.KitPvP.Events.FFA.Listeners.FFApatch;
import me.Hackusate_PvP.KitPvP.Events.TDM.Commands.TDM;
import me.Hackusate_PvP.KitPvP.Events.TDM.Listeners.TDMPatch;
import me.Hackusate_PvP.KitPvP.Fixes.ChatFormat;
import me.Hackusate_PvP.KitPvP.Fixes.JoinEvent;
import me.Hackusate_PvP.KitPvP.Fixes.Patches;
import me.Hackusate_PvP.KitPvP.PvP.ArrowIndicater;
import me.Hackusate_PvP.KitPvP.PvP.DeathMessages;
import me.Hackusate_PvP.KitPvP.PvP.MoneyPerKill;
import me.Hackusate_PvP.KitPvP.PvP.PearlCooldown;
import me.Hackusate_PvP.KitPvP.Settings.Inv;
import me.Hackusate_PvP.KitPvP.Settings.Settings;
import me.Hackusate_PvP.KitPvP.Staff.commands.*;
import me.Hackusate_PvP.KitPvP.Staff.events.FreezeEvent;
import me.Hackusate_PvP.KitPvP.Staff.events.StaffModeEvent;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Utils.Staff;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import net.minecraft.server.v1_7_R4.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.CommandMap;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;


import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Main extends JavaPlugin {
    private static final Logger log = Logger.getLogger("Minecraft");
    private static Economy econ = null;
    private static Permission perms = null;
    private static Chat chat = null;
    private FreezeEvent freezeListener;
    public static FileConfiguration config;

    public static Main plugin;
    public static Main instance;
    public StaffModeEvent staffModeListener;
    public static YamlConfiguration LANG;
    public static YamlConfiguration STAFF;
    public static File LANG_FILE;
    public static File STAFF_FILE;
    public static Connection connection;
    private String host, database, username, password;
    private int port;
    private File text = new File("perm.txt");
    private final String name = "core";

    public boolean connected;
    private PluginDescriptionFile pdf;

    public void onEnable() {
        instance = this;
        plugin = this;
        log.info("[Core] loading...");
        if (!(this.getDescription().getName().equalsIgnoreCase("Core"))) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_RED + "DO NOT CHANGE PLUGIN.YML WEEB");
            Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_RED + "DO NOT CHANGE PLUGIN.YML WEEB");
            Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_RED + "DO NOT CHANGE PLUGIN.YML WEEB");
            Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_RED + "DO NOT CHANGE PLUGIN.YML WEEB");
            Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_RED + "DO NOT CHANGE PLUGIN.YML WEEB");
            Bukkit.getConsoleSender().sendMessage(ChatColor.DARK_RED + "DO NOT CHANGE PLUGIN.YML WEEB");
            Bukkit.getServer().shutdown();
        }
        log.info("[Core] Loading config...");
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
        this.saveDefaultConfig();
        this.reloadConfig();
        config = getConfig();
        log.info("[Core] Loading lang.yml...");
        this.loadLang();
        log.info("[Core] Loading staff.yml...");
        this.loadStaff();
        this.ffastart();
        this.ffapvp();
       // log.info("[Core] checking HWID...");
        //this.checkHWID();
        log.info("[Core] Checking Version...");
        try {
            Thread.sleep(10, 10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.checkVersion();
        log.info("[Core] Loading database...");
        host = Main.config.getString("DataBase.MYSQL.Host");
        port = Main.config.getInt("DataBase.MYSQL.Port");
        database = Main.config.getString("DataBase.MYSQL.Database");
        username = Main.config.getString("DataBase.MYSQL.User");
        password = Main.config.getString("DataBase.MYSQL.Password");
        log.info("[Core] Clearing data...");
        CoreAPI.clearList();
        log.info("[Core] Loading events...");
        this.registerFixes();
        this.registerPvPEvents();
        this.registerGameEvents();
        log.info("[Core] Loading scoreboard...");
        new Aether(this, new BoardLink(this));
        log.info("[Core] Loading broadcaster...");
        this.broadcast();
        log.info("[Core] Loading hooks...");
        if (!setupEconomy()) {
            log.warning("[Core] Economy plugin not found.");
        }

        if (!setupPermissions()) {
            log.severe("[Core] ERROR Permission plugin not found. Disabling...");
            log.severe("[Core] ERROR Permission plugin not found. Disabling...");
            log.severe("[Core] ERROR Permission plugin not found. Disabling...");
            getServer().getPluginManager().disablePlugin(this);
        }
        log.info("[Core] Loading commands...");
        this.registerCommands();
        log.info("[Core] Loading staff commands");
        this.registerStaffCMD();
        this.registerStaffEvents();
        log.info("[Core] Done.");

    }

    private void checkVersion() {
        try {
            String web = "https://pastebin.com/raw/HLLC7A4N";
            URL url = new URL(web);
            ArrayList<Object> lines = new ArrayList<>();
            URLConnection connection = url.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
            if (!lines.contains("1.0")) {
                log.info("Update found please download!");
                Bukkit.getServer().shutdown();
            }
        } catch (Exception var6) {
            log.info("Version unknown. Shutting down.");
            Bukkit.getServer().shutdown();
        }
    }

    public void openConnection() throws SQLException, ClassNotFoundException {
        if (connection != null && !connection.isClosed()) {
            return;
        }

        synchronized (this) {
            if (connection != null && !connection.isClosed()) {
                return;
            }
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.host+ ":" + this.port + "/" + this.database, this.username, this.password);
        }
    }

    public void onDisable() {
        log.info("[Core] Disabling");
        CoreAPI.clearList();

        // initiate last
        Iterator var2 = Bukkit.getServer().getOnlinePlayers().iterator();

        while(var2.hasNext()) {
            Player player = (Player) var2.next();
            if (this.staffModeListener.isStaffModeActive(player)) {
                this.staffModeListener.setStaffMode(player, false);
                player.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cYour Staff Mode was disabled due a Server Reload by an operator."));
            }
        }
        plugin = null;
    }

    public static String getMainColor() {
        return config.getString("Core.MainColor");
    }

    public static String getAltColor() {
        return Main.config.getString("Core.AltColor");
    }

    public static String getOther() {
        return Main.config.getString("Core.Other");
    }

    public static String getSymbol() {
        return Main.config.getString("Core.Symbol");
    }

    public static String getBar() {
        return Main.config.getString("Core.Bar");
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> rsp = getServer().getServicesManager().getRegistration(Chat.class);
        chat = rsp.getProvider();
        return chat != null;
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        perms = rsp.getProvider();
        return perms != null;
    }

    private void broadcast() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (!(CoreAPI.hasBroadcastDisabled(player))) {
                        Random object = new Random();
                        int num;
                        for (int counter = 1; counter <= 1; counter++) {
                            if (Main.config.getInt("Settings.Broadcast.Messages") <= 10) {
                                num = 1 + object.nextInt(Main.config.getInt("Settings.Broadcast.Messages"));
                                if (Main.config.getBoolean("Settings.Broadcast.Enabled")) {
                                    if (!(Main.config.getString("Broadcast.Messages.Message1").equals(""))) {
                                        if (num == 1) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message1")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message2").equals(""))) {
                                        if (num == 2) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message2")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message3").equals(""))) {
                                        if (num == 3) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message3")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message4").equals(""))) {
                                        if (num == 4) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message4")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message5").equals(""))) {
                                        if (num == 5) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message5")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message6").equals(""))) {
                                        if (num == 6) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message6")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message7").equals(""))) {
                                        if (num == 7) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message7")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message8").equals(""))) {
                                        if (num == 8) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message8")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message9").equals(""))) {
                                        if (num == 9) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message9")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }

                                    if (!(Main.config.getString("Broadcast.Messages.Message10").equals(""))) {
                                        if (num == 10) {
                                            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.config.getString("Broadcast.Messages.Message10")));
                                            if (Main.config.getBoolean("Settings.Broadcast.Sound")) {
                                                player.playSound(player.getLocation(), Sound.NOTE_PLING, 1, 1);
                                            }
                                        }
                                    }
                                }
                            } else {
                                Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "[Alert] you need to have less than 11 for broadcast messages");
                            }
                        }
                    }
                }
            }
        }, 0L, Main.config.getInt("Settings.Broadcast.Delay"));
    }

    public void ffastart() {
        final int[] ffatime = {0};
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                if (FFA.ffastart == 1) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (ffatime[0] == 0) {
                            log.info("[DEV] " + ffatime[0]);
                            player.sendMessage(ChatColor.GREEN + "The event is starting in 30 seconds!");
                        }
                        if (ffatime[0] < 30) {
                            log.info("[DEV] " + ffatime[0]);
                            ffatime[0] = ffatime[0] + 1;
                        }
                        if (ffatime[0] == 15) {
                            player.sendMessage(ChatColor.GOLD + "Starting in 15 seconds");
                            log.info("[DEV] " + ffatime[0]);
                        }
                        if (ffatime[0] == 20) {
                            player.sendMessage(ChatColor.GOLD + "Starting in 10 seconds");
                            log.info("[DEV] " + ffatime[0]);
                        }
                        if (ffatime[0] == 25) {
                            player.sendMessage(ChatColor.GOLD + "Starting in 5 seconds");
                        }
                        if (ffatime[0] == 25) {
                            player.sendMessage(ChatColor.RED + "Starting in " + ChatColor.GOLD + "5");
                        }
                        if (ffatime[0] == 26) {
                            player.sendMessage(ChatColor.RED + "Starting in " + ChatColor.GOLD + "4");
                        }
                        if (ffatime[0] == 27) {
                            player.sendMessage(ChatColor.RED + "Starting in " + ChatColor.GOLD + "3");
                        }
                        if (ffatime[0] == 28) {
                            player.sendMessage(ChatColor.RED + "Starting in " + ChatColor.GOLD + "2");
                        }
                        if (ffatime[0] == 29) {
                            player.sendMessage(ChatColor.RED + "Starting in " + ChatColor.GOLD + "1");
                        }
                        if (ffatime[0] == 30) {
                            if (CoreAPI.isJoinedPlayer(player)) {
                                CoreAPI.setQueue(player, true, CoreAPI.kit);
                                FFA.ffastart = 0;
                            }
                        }
                    }
                }
            }
        }, 0L, 20L);
    }

    public static Location getSpawn() {
        return new Location(Bukkit.getWorld(config.getString("Spawn.World")), config.getInt("Spawn.X"), config.getInt("Spawn.Y"), config.getInt("Spawn.Z"));
    }

    public void ffapvp() {
        final int[] pvptime = {0};
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            public void run() {
                if (CoreAPI.pvp == 1) {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (CoreAPI.isJoinedPlayer(player)) {
                            if (pvptime[0] == 0) {
                                pvptime[0] = pvptime[0] + 1;
                                player.sendMessage(ChatColor.RED + "5");
                            } else if (pvptime[0] == 1) {
                                pvptime[0] = pvptime[0] + 1;
                                player.sendMessage(ChatColor.RED + "4");
                            } else if (pvptime[0] == 2) {
                                pvptime[0] = pvptime[0] + 1;
                                player.sendMessage(ChatColor.RED + "3");
                            } else if (pvptime[0] == 3) {
                                pvptime[0] = pvptime[0] + 1;
                                player.sendMessage(ChatColor.RED + "2");
                            } else if (pvptime[0] == 4) {
                                pvptime[0] = pvptime[0] + 1;
                                player.sendMessage(ChatColor.RED + "1");
                            } else if (pvptime[0] == 5) {
                                pvptime[0] = pvptime[0] + 1;
                                player.sendMessage(ChatColor.RED + "GO");
                                CoreAPI.pvp = 0;
                            }
                        }
                    }
                }
            }
        }, 0L, 20L);
    }

    /**
     * Load the lang.yml file.
     * @return The lang.yml config.
     */
    public YamlConfiguration loadLang() {
        File lang = new File(getDataFolder(), "lang.yml");
        if (!lang.exists()) {
            try {
                getDataFolder().mkdir();
                lang.createNewFile();
                InputStream defConfigStream = this.getResource("lang.yml");
                if (defConfigStream != null) {
                    YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
                    defConfig.save(lang);
                    Lang.setFile(defConfig);
                    return defConfig;
                }
            } catch (IOException e) {
                e.printStackTrace(); // So they notice
                log.severe("[PluginName] Couldn't create language file.");
                log.severe("[PluginName] This is a fatal error. Now disabling");
                this.setEnabled(false); // Without it loaded, we can't send them messages
            }
        }
        YamlConfiguration conf = YamlConfiguration.loadConfiguration(lang);
        for (Lang item : Lang.values()) {
            if (conf.getString(item.getPath()) == null) {
                conf.set(item.getPath(), item.getDefault());
            }
        }
        Lang.setFile(conf);
        Main.LANG = conf;
        Main.LANG_FILE = lang;
        try {
            conf.save(getLangFile());
        } catch (IOException e) {
            log.log(Level.WARNING, "PluginName: Failed to save lang.yml.");
            log.log(Level.WARNING, "PluginName: Report this stack trace to <your name>.");
            e.printStackTrace();
        }
        return conf;
    }

    public YamlConfiguration loadStaff() {
        File staff = new File(getDataFolder(), "staff.yml");
        if (!staff.exists()) {
            try {
                getDataFolder().mkdir();
                staff.createNewFile();
                InputStream defConfigStream = this.getResource("staff.yml");
                if (defConfigStream != null) {
                    YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
                    defConfig.save(staff);
                    Staff.setFile(defConfig);
                    return defConfig;
                }
            } catch (IOException e) {
                e.printStackTrace(); // So they notice
                log.severe("[PluginName] Couldn't create language file.");
                log.severe("[PluginName] This is a fatal error. Now disabling");
                this.setEnabled(false); // Without it loaded, we can't send them messages
            }
        }
        YamlConfiguration conf = YamlConfiguration.loadConfiguration(staff);
        for (Staff item : Staff.values()) {
            if (conf.getString(item.getPath()) == null) {
                conf.set(item.getPath(), item.getDefault());
            }
        }
        Staff.setFile(conf);
        Main.STAFF = conf;
        Main.STAFF_FILE = staff;
        try {
            conf.save(getStaffFile());
        } catch (IOException e) {
            log.log(Level.WARNING, "PluginName: Failed to save staff.yml.");
            log.log(Level.WARNING, "PluginName: Report this stack trace to <your name>.");
            e.printStackTrace();
        }
        return conf;
    }

    /**
     * Gets the lang.yml config.
     * @return The lang.yml config.
     */
    public YamlConfiguration getLang() {
        return LANG;
    }

    /**
     * Get the lang.yml file.
     * @return The lang.yml file.
     */
    public File getLangFile() {
        return LANG_FILE;
    }

    public File getStaffFile() {
        return STAFF_FILE;
    }

    private void registerCommands() {
        if (Main.config.getBoolean("Commands.FFA")) {
            getCommand("ffa").setExecutor(new FFA());
        }
        if (Main.config.getBoolean("Commands.Core")) {
            getCommand("core").setExecutor(new Core());
        }
        if (Main.config.getBoolean("Commands.Help")) {
            getCommand("help").setExecutor(new Help());
        }
        if (setupEconomy()) {
            if (Main.config.getBoolean("Commands.FixHand")) {
                getCommand("fixhand").setExecutor(new FixHand());
            }
        }
        if (Main.config.getBoolean("Commands.EChest")) {
            getCommand("echest").setExecutor(new EChest());
        }
        if (Main.config.getBoolean("Commands.ToggleBlocks")) {
            getCommand("toggleblocks").setExecutor(new ToggleBlocks());
        }
        if (Main.config.getBoolean("Commands.Report")) {
            getCommand("report").setExecutor(new Report());
        }
        if (Main.config.getBoolean("Commands.Feed")) {
            getCommand("feed").setExecutor(new Feed());
        }
        if (Main.config.getBoolean("Commands.Permission")) {
            getCommand("permission").setExecutor(new Permissin());
        }
        if (Main.config.getBoolean("Commands.Creative")) {
            getCommand("creative").setExecutor(new Creative());
        }
        if (Main.config.getBoolean("Commands.Survival")) {
            getCommand("survival").setExecutor(new Survival());
        }
        if (Main.config.getBoolean("Commands.Fly")) {
            getCommand("fly").setExecutor(new Fly());
        }
        if (Main.config.getBoolean("Commands.Settings")) {
            getCommand("settings").setExecutor(new Settings());
        }
    }

    private void RegisterCommands() {
        // MinecraftServer.getServer().server.getCommandMap().register("test" , new test("test"));
    }

    private void registerStaffCMD() {

        if (Main.config.getBoolean("Settings.Staff")) {
            if (Main.config.getBoolean("Commands.StaffMode")) {
                getCommand("staffmode").setExecutor(new StaffCommand());
            }
            if (Main.config.getBoolean("Commands.Vanish")) {
                getCommand("vanish").setExecutor(new VanishCommand());
            }

            if (Main.config.getBoolean("Commands.Freeze")) {
                getCommand("freeze").setExecutor(new FreezeCommand());
            }

            if (Main.config.getBoolean("Commands.ClearChat")) {
                getCommand("clearchat").setExecutor(new ClearChatCommand());
            }
            if (Main.config.getBoolean("Commands.Inspect")) {
                getCommand("inspect").setExecutor(new InspectionCommand());
            }
            if (Main.config.getBoolean("Commands.Teleport")) {
                getCommand("teleport").setExecutor(new TeleportCommand());
            }
        }
    }
    private void registerStaffEvents() {
        PluginManager manager = this.getServer().getPluginManager();
        if (Main.config.getBoolean("Settings.Staff")) {
            manager.registerEvents(new FreezeEvent(), this);
            this.staffModeListener = new StaffModeEvent();
            Bukkit.getServer().getPluginManager().registerEvents(this.staffModeListener, this);
            this.freezeListener = new FreezeEvent();
            Bukkit.getServer().getPluginManager().registerEvents(this.freezeListener, this);
        }
    }

    private void registerFixes() {
        PluginManager manager = this.getServer().getPluginManager();
        manager.registerEvents(new JoinEvent(), this);
        manager.registerEvents(new Patches(), this);
        manager.registerEvents(new ToggleBlocks(), this);
        manager.registerEvents(new Inv(), this);
        manager.registerEvents(new ChatFormat(), this);
        manager.registerEvents(new FFApatch(), this);
        manager.registerEvents(new ArrowIndicater(), this);
    }

    private void registerPvPEvents() {
        PluginManager manager = this.getServer().getPluginManager();
        if (Main.config.getBoolean("Events.Death-Messages")) {
            manager.registerEvents(new DeathMessages(), this);
        }

        if (Main.config.getBoolean("Events.Pearl-Cooldown")) {
            manager.registerEvents(new PearlCooldown(), this);
        }
        if (setupEconomy()) {
            if (Main.config.getBoolean("Events.Money-Per-Kill")) {
                manager.registerEvents(new MoneyPerKill(), this);
            }
        }
    }

    private void registerGameEvents() {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        if (config.getBoolean("Game.Events.TDM")) {
            getCommand("tdm").setExecutor(new TDM());
            manager.registerEvents(new TDMPatch(), this);
        }
    }

    public static Economy getEconomy() {
        return econ;
    }

    public static Permission getPermissions() {
        return perms;
    }

    public static Chat getChat() {
        return chat;
    }

    public FreezeEvent getFreezeListener() {
        return this.freezeListener;
    }

    public StaffModeEvent getStaffModeListener() {
        return this.staffModeListener;
    }



    public static Main getInstance() {
        return instance;
    }
}
