package me.Hackusate_PvP.KitPvP.Utils;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import net.minecraft.server.v1_7_R4.MinecraftServer;
import net.minecraft.util.gnu.trove.list.TCharList;
import net.minecraft.util.gnu.trove.list.array.TCharArrayList;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.projectiles.ProjectileSource;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class BukkitUtils {
    private static final int DEFAULT_COMPLETION_LIMIT = 80;
    private static final String STRAIGHT_LINE_TEMPLATE;
    public static final String STRAIGHT_LINE_DEFAULT;
    private static final TCharList COLOUR_CHARACTER_LIST;

    public BukkitUtils() {
    }

    public static int countColoursUsed(String id, boolean ignoreDuplicates) {
        int count = 0;
        Set<ChatColor> found = new HashSet();

        for(int i = 1; i < id.length(); ++i) {
            char current = id.charAt(i);
            if (COLOUR_CHARACTER_LIST.contains(current) && id.charAt(i - 1) == '&' && (ignoreDuplicates || found.add(ChatColor.getByChar(current)))) {
                ++count;
            }
        }

        return count;
    }

    public static List<String> getCompletions(String[] args, List<String> input) {
        return getCompletions(args, input, 80);
    }

    public static List<String> getCompletions(String[] args, List<String> input, int limit) {
        Preconditions.checkNotNull(args);
        Preconditions.checkArgument(args.length != 0);
        String argument = args[args.length - 1];
        return (List)input.stream().filter((string) -> {
            return string.regionMatches(true, 0, argument, 0, argument.length());
        }).limit((long)limit).collect(Collectors.toList());
    }

    public static String getDisplayName(CommandSender sender) {
        Preconditions.checkNotNull(sender);
        return sender instanceof Player ? ((Player)sender).getDisplayName() : sender.getName();
    }

    public static long getIdleTime(Player player) {
        Preconditions.checkNotNull(player);
        long idleTime = ((CraftPlayer)player).getHandle().x();
        return idleTime > 0L ? MinecraftServer.ar() - idleTime : 0L;
    }


    public static Player getFinalAttacker(EntityDamageEvent ede, boolean ignoreSelf) {
        Player attacker = null;
        if (ede instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent event = (EntityDamageByEntityEvent)ede;
            Entity damager = event.getDamager();
            if (event.getDamager() instanceof Player) {
                attacker = (Player)damager;
            } else if (event.getDamager() instanceof Projectile) {
                Projectile projectile = (Projectile)damager;
                ProjectileSource shooter = projectile.getShooter();
                if (shooter instanceof Player) {
                    attacker = (Player)shooter;
                }
            }

            if (attacker != null && ignoreSelf && event.getEntity().equals(attacker)) {
                attacker = null;
            }
        }

        return attacker;
    }

    public static boolean isWithinX(Location location, Location other, double distance) {
        return location.getWorld().equals(other.getWorld()) && Math.abs(other.getX() - location.getX()) <= distance && Math.abs(other.getZ() - location.getZ()) <= distance;
    }

    public static Location getHighestLocation(Location origin) {
        return getHighestLocation(origin, (Location)null);
    }

    public static Location getHighestLocation(Location origin, Location def) {
        Preconditions.checkNotNull(origin, "The location cannot be null");
        Location cloned = origin.clone();
        World world = cloned.getWorld();
        int x = cloned.getBlockX();
        int y = world.getMaxHeight();
        int z = cloned.getBlockZ();

        Block block;
        do {
            if (y <= origin.getBlockY()) {
                return def;
            }

            --y;
            block = world.getBlockAt(x, y, z);
        } while(block.isEmpty());

        Location next = block.getLocation();
        next.setPitch(origin.getPitch());
        next.setYaw(origin.getYaw());
        return next;
    }




    static {
        STRAIGHT_LINE_TEMPLATE = ChatColor.STRIKETHROUGH.toString() + Strings.repeat("-", 256);
        STRAIGHT_LINE_DEFAULT = STRAIGHT_LINE_TEMPLATE.substring(0, 55);
        ChatColor[] values = ChatColor.values();
        COLOUR_CHARACTER_LIST = new TCharArrayList(values.length);
        ChatColor[] var1 = values;
        int var2 = values.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            ChatColor colour = var1[var3];
            COLOUR_CHARACTER_LIST.add(colour.getChar());
        }

    }
}

