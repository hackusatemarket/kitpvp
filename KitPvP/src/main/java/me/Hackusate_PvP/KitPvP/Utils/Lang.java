package me.Hackusate_PvP.KitPvP.Utils;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * An enum for requesting strings from the language file.
 * @author gomeow
 */
public enum Lang {
    TITLE("title-name", "[Core]"),
    INVALD_ARGS("invalid-args", "&cIncorrect usage."),
    EVENT_STARTED("event-already-started", "&cEvent already started"),
    REPORT_SELF("report-self", "&cYou cannot report yourself."),
    PLAYER_NOT_FOUND("player-not-found", "&cPlayer not found."),
    ECHEST_OPENED("enderchest-open", "&aYou have opened your enderchest."),
    TOGGLE_BLOCK_ENABLED("toggleblocks-enabled", "&aYou have enabled block breaking/building."),
    TOGGLE_BLOCK_DISABLED("toggleblocks-disabled", "&cYou have disabled block breaking/building."),
    DEATH_MESSAGE_KILLER_PLAYER("player-death-message", "&6%player% &7 was killed by &6%killer%"),
    DEATH_MESSAGE_KILLER_OTHER("player-other-death-message", "&6%player% &7was killed by something else"),
    FEED_PLAYER("feed-player", "&aYou have been fed."),
    FEED_OTHER("feed-other", "&aYou have fed &l%target%"),
    FEED_TARGET("feed-target", "&aYou have been fed by &l%player%"),
    MONEY_EARNED_MESSAGE("money-earned-message", "&7You have received &a%amount%"),
    NO_PERMS("no-permissions", "&cSorry you do not have permission."),
    GAMEMODE_MESSAGE("gamemode-message", "&7Your gamemode has been set to &6%gm%"),
    GAMEMODE_MESSAGE_SENDER("gamemode-message-sender", "&7You have set the gamemode of &c%target% &7to &6%gm%"),
    GAMEMODE_MESSAGE_TARGET("gamemode-message-target", "&6%sender% &7has set your gamemode to &6%gm%"),
    FLY_ENABLED("fly-enabled", "&7You have &aenabled &7flight."),
    FLY_DSIABLED("fly-disabled", "&7You have &cdisabled &7flight."),
    ;

    private String path;
    private String def;
    private static YamlConfiguration LANG;

    /**
     * Lang enum constructor.
     * @param path The string path.
     * @param start The default string.
     */
    Lang(String path, String start) {
        this.path = path;
        this.def = start;
    }

    /**
     * Set the {@code YamlConfiguration} to use.
     * @param config The config to set.
     */
    public static void setFile(YamlConfiguration config) {
        LANG = config;
    }

    @Override
    public String toString() {
        if (this == TITLE)
            return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def)) + " ";
        return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, def));
    }

    /**
     * Get the default value of the path.
     * @return The default value of the path.
     */
    public String getDefault() {
        return this.def;
    }

    /**
     * Get the path to the string.
     * @return The path to the string.
     */
    public String getPath() {
        return this.path;
    }
}
