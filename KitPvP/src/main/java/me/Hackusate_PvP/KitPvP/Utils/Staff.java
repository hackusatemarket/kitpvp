package me.Hackusate_PvP.KitPvP.Utils;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * An enum for requesting strings from the language file.
 * @author gomeow
 */
public enum Staff {
    TITLE("title-name", "[Core]"),
    STAFF_ALERT("staff-alert", "&c[Alert] &6%target% &7is now frozen, frozen by &a%player%&7."),
    STAFF_FREEZE_PLAYER("staff-freeze-player", "&7You have frozen &6&l %target%"),
    STAFF_ALERT_FREEZE_MESSAGE("staff-alert-freeze-message", "&c[Staff] &6%target% &7has now been frozen by &6%player%&7."),
    STAFF_FROZE_MESSAGE("staff-alert-froze-message", "&6%target% &7is now frozen."),
    STAFF_ALERT_SET_FROZE_MESSAGE("staff-alert-set-froze-message", "&c[Staff] &6%target% &7is now frozen, frozen by &6%player%&7.'"),
    STAFF_UNFREEZE_PLAYER("staff-unfreeze-message", "&6You are no longer frozen."),
    STAFF_ALERT_UNFREEZE_PLAYER("staff-alert-unfreeze-player", "&c[Staff] &6%target% &7is no longer frozen, removed by &6%player%&e."),
    STAFF_UNFROZEN_STAFF_MESSAGE("staff-unfrozen-player-message", "&6%target% &7is now unfrozen."),
    ENTER_STAFF("enter-staff", "&aStaff Connected: &f%player%."),
    LEAVE_STAFF("leave-staff", "caStaff Disconnected: &f%player%."),
    STAFF_MODE_ENTER("staff-mode-enter", "&7Your staff mode has been &aenabled&e."),
    STAFF_MODE_LEAVE("staff-mode-leave", "&7Your staff mode has been &cdisabled&e."),
    STAFF_FREEZE("staff-freeze", "&cYou can not freeze an Staff Member."),
    STAFF_FREEZE_SELF("staff-freeze-self", "&cYou can not freeze yourself."),
    REPORT_STAFF_MESSAGE("report-staff-message", "&6%target% &7has been reported by &a%sender% &6Reason: &c%reason%"),
    VANISH_ENABLED_MESSAGE("vanish-enabled-message", "&aYou are now in vanish."),
    VANISH_DISABLED_MESSAGE("vanish-disabled-message", "&cYou are no longer in vanish."),
    TELEPORT_FAILED("teleport-failed", "&cCould not find players to teleport."),
    TELEPORT_TO_PLAYER("teleport-to-player", "&7You have teleported to &6%target%"),
    TELEPORT_PLAYER_TO_PLAYER("teleport-player-to-player", "&6%sender% &7has teleported you to &c%target%"),
    TELEPORT_PLAYER_ALERT("teleport-player-alert", "&c[Staff] &6%sender% &7has teleport to &c%target%"),
    TELEPORT_PLAYER_TO_COORDS("teleport-player-to-coords", "&7You have teleported &c%target% &7to &6%x%&f,&6%y%&f,&6%z%"),
    TELEPORT_PLAYER_TO_COORDS_TARGET("teleport-player-to-coords-target", "&6%sender% &7has teleported you to &6%x%&f,&6%y%&f,&6%z%"),
    TELEPORT_PLAYER_TO_COORDS_ALERT("teleport-player-to-coords-alert", "&c[Staff] &6%sender% &7has teleported &c%target% &7to &6%x%&f,&6%y%&f,&6%z%"),
    TELEPORT_PLAYER_TO_PLAYER_ALERT("teleport-player-to-player-alert", "&c[Staff] &6%sender% &7has teleported &c%target% &7to &c%target2%"),
    TELEPORT_SENDER_TO_COORDS("teleport-sender-to-coords", "&7You have been teleported to &6%x%&f,&6%y%&f,&6%z%"),
    TELEPORT_SENDER_TO_COORDS_ALERT("teleport-sender-to-coords-alert", "&c[Staff] &6%sender% &7has teleported &7to &6%x%&f,&6%y%&f,&6%z%"),
    ;



    private String path;
    private String def;
    private static YamlConfiguration STAFF;

    /**
     * Lang enum constructor.
     * @param path The string path.
     * @param start The default string.
     */
    Staff(String path, String start) {
        this.path = path;
        this.def = start;
    }

    /**
     * Set the {@code YamlConfiguration} to use.
     * @param config The config to set.
     */
    public static void setFile(YamlConfiguration config) {
        STAFF = config;
    }

    @Override
    public String toString() {
        if (this == TITLE)
            return ChatColor.translateAlternateColorCodes('&', STAFF.getString(this.path, def)) + " ";
        return ChatColor.translateAlternateColorCodes('&', STAFF.getString(this.path, def));
    }

    /**
     * Get the default value of the path.
     * @return The default value of the path.
     */
    public String getDefault() {
        return this.def;
    }

    /**
     * Get the path to the string.
     * @return The path to the string.
     */
    public String getPath() {
        return this.path;
    }
}
