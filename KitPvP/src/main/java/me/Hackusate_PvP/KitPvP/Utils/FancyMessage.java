package me.Hackusate_PvP.KitPvP.Utils;

import org.bukkit.Achievement;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Statistic;
import org.bukkit.Statistic.Type;
import org.bukkit.craftbukkit.libs.com.google.gson.stream.JsonWriter;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FancyMessage {
    private final List<MessagePart> messageParts = new ArrayList();
    private String jsonString;
    private boolean dirty;
    private Class<?> nmsChatSerializer = Reflection.getNMSClass("ChatSerializer");
    private Class<?> nmsTagCompound = Reflection.getNMSClass("NBTTagCompound");
    private Class<?> nmsPacketPlayOutChat = Reflection.getNMSClass("PacketPlayOutChat");
    private Class<?> nmsAchievement = Reflection.getNMSClass("Achievement");
    private Class<?> nmsStatistic = Reflection.getNMSClass("Statistic");
    private Class<?> nmsItemStack = Reflection.getNMSClass("ItemStack");
    private Class<?> obcStatistic = Reflection.getOBCClass("CraftStatistic");
    private Class<?> obcItemStack = Reflection.getOBCClass("inventory.CraftItemStack");

    public FancyMessage(String firstPartText) {
        this.messageParts.add(new MessagePart(firstPartText));
        this.jsonString = null;
        this.dirty = false;
    }

    public FancyMessage color(ChatColor color) {
        if (!color.isColor()) {
            throw new IllegalArgumentException(color.name() + " is not a color");
        } else {
            this.latest().color = color;
            this.dirty = true;
            return this;
        }
    }

    public FancyMessage style(ChatColor... styles) {
        ChatColor[] arrayOfChatColor = styles;
        int j = styles.length;

        for(int i = 0; i < j; ++i) {
            ChatColor style = arrayOfChatColor[i];
            if (!style.isFormat()) {
                throw new IllegalArgumentException(style.name() + " is not a style");
            }
        }

        this.latest().styles = styles;
        this.dirty = true;
        return this;
    }

    public FancyMessage file(String path) {
        this.onClick("open_file", path);
        return this;
    }

    public FancyMessage link(String url) {
        this.onClick("open_url", url);
        return this;
    }

    public FancyMessage suggest(String command) {
        this.onClick("suggest_command", command);
        return this;
    }

    public FancyMessage command(String command) {
        this.onClick("run_command", command);
        return this;
    }

    public FancyMessage achievementTooltip(String name) {
        this.onHover("show_achievement", "achievement." + name);
        return this;
    }

    public FancyMessage achievementTooltip(Achievement which) {
        try {
            Object achievement = Reflection.getMethod(this.obcStatistic, "getNMSAchievement", new Class[0]).invoke((Object)null, which);
            return this.achievementTooltip((String) Reflection.getField(this.nmsAchievement, "name").get(achievement));
        } catch (Exception var3) {
            var3.printStackTrace();
            return this;
        }
    }

    public FancyMessage statisticTooltip(Statistic which) {
        Type type = which.getType();
        if (type != Type.UNTYPED) {
            throw new IllegalArgumentException("That statistic requires an additional " + type + " parameter!");
        } else {
            try {
                Object statistic = Reflection.getMethod(this.obcStatistic, "getNMSStatistic", new Class[0]).invoke((Object)null, which);
                return this.achievementTooltip((String) Reflection.getField(this.nmsStatistic, "name").get(statistic));
            } catch (Exception var4) {
                var4.printStackTrace();
                return this;
            }
        }
    }

    public FancyMessage statisticTooltip(Statistic which, Material item) {
        Type type = which.getType();
        if (type == Type.UNTYPED) {
            throw new IllegalArgumentException("That statistic needs no additional parameter!");
        } else if ((type != Type.BLOCK || !item.isBlock()) && type != Type.ENTITY) {
            try {
                Object statistic = Reflection.getMethod(this.obcStatistic, "getMaterialStatistic", new Class[0]).invoke((Object)null, which, item);
                return this.achievementTooltip((String) Reflection.getField(this.nmsStatistic, "name").get(statistic));
            } catch (Exception var5) {
                var5.printStackTrace();
                return this;
            }
        } else {
            throw new IllegalArgumentException("Wrong parameter type for that statistic - needs " + type + "!");
        }
    }

    public FancyMessage statisticTooltip(Statistic which, EntityType entity) {
        Type type = which.getType();
        if (type == Type.UNTYPED) {
            throw new IllegalArgumentException("That statistic needs no additional parameter!");
        } else if (type != Type.ENTITY) {
            throw new IllegalArgumentException("Wrong parameter type for that statistic - needs " + type + "!");
        } else {
            try {
                Object statistic = Reflection.getMethod(this.obcStatistic, "getEntityStatistic", new Class[0]).invoke((Object)null, which, entity);
                return this.achievementTooltip((String) Reflection.getField(this.nmsStatistic, "name").get(statistic));
            } catch (Exception var5) {
                var5.printStackTrace();
                return this;
            }
        }
    }

    public FancyMessage itemTooltip(String itemJSON) {
        this.onHover("show_item", itemJSON);
        return this;
    }

    public FancyMessage itemTooltip(ItemStack itemStack) {
        try {
            Object nmsItem = Reflection.getMethod(this.obcItemStack, "asNMSCopy", new Class[]{ItemStack.class}).invoke((Object)null, itemStack);
            return this.itemTooltip(Reflection.getMethod(this.nmsItemStack, "save", new Class[0]).invoke(nmsItem, this.nmsTagCompound.newInstance()).toString());
        } catch (Exception var3) {
            var3.printStackTrace();
            return this;
        }
    }

    public FancyMessage tooltip(String text) {
        return this.tooltip(text.split("\\n"));
    }

    public FancyMessage tooltip(List<String> lines) {
        return this.tooltip((String[])lines.toArray());
    }

    public FancyMessage tooltip(String... lines) {
        if (lines.length == 1) {
            this.onHover("show_text", lines[0]);
        } else {
            this.itemTooltip(this.makeMultilineTooltip(lines));
        }

        return this;
    }

    public FancyMessage then(Object obj) {
        this.messageParts.add(new MessagePart(obj.toString()));
        this.dirty = true;
        return this;
    }

    public String toJSONString() {
        if (!this.dirty && this.jsonString != null) {
            return this.jsonString;
        } else {
            StringWriter string = new StringWriter();
            JsonWriter json = new JsonWriter(string);

            try {
                if (this.messageParts.size() == 1) {
                    this.latest().writeJson(json);
                } else {
                    json.beginObject().name("text").value("").name("extra").beginArray();
                    Iterator var4 = this.messageParts.iterator();

                    while(var4.hasNext()) {
                        MessagePart messagePart = (MessagePart)var4.next();
                        messagePart.writeJson(json);
                    }

                    json.endArray().endObject();
                    json.close();
                }
            } catch (Exception var5) {
                throw new RuntimeException("invalid message");
            }

            this.jsonString = string.toString();
            this.dirty = false;
            return this.jsonString;
        }
    }

    public void send(Player player) {
        try {
            Object handle = Reflection.getHandle(player);
            Object connection = Reflection.getField(handle.getClass(), "playerConnection").get(handle);
            Object serialized = Reflection.getMethod(this.nmsChatSerializer, "a", new Class[]{String.class}).invoke((Object)null, this.toJSONString());
            Object packet = this.nmsPacketPlayOutChat.getConstructor(Reflection.getNMSClass("IChatBaseComponent")).newInstance(serialized);
            Reflection.getMethod(connection.getClass(), "sendPacket", new Class[0]).invoke(connection, packet);
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    public void send(Iterable<Player> players) {
        Iterator var3 = players.iterator();

        while(var3.hasNext()) {
            Player player = (Player)var3.next();
            this.send(player);
        }

    }

    private MessagePart latest() {
        return (MessagePart)this.messageParts.get(this.messageParts.size() - 1);
    }

    private String makeMultilineTooltip(String[] lines) {
        StringWriter string = new StringWriter();
        JsonWriter json = new JsonWriter(string);

        try {
            json.beginObject().name("id").value(1L);
            json.name("tag").beginObject().name("display").beginObject();
            json.name("Name").value("\\u00A7f" + lines[0].replace("\"", "\\\""));
            json.name("Lore").beginArray();

            for(int i = 1; i < lines.length; ++i) {
                String line = lines[i];
                json.value(line.isEmpty() ? " " : line.replace("\"", "\\\""));
            }

            json.endArray().endObject().endObject().endObject();
            json.close();
            return string.toString();
        } catch (Exception var6) {
            throw new RuntimeException("invalid tooltip");
        }
    }

    public void onClick(String name, String data) {
        MessagePart latest = this.latest();
        latest.clickActionName = name;
        latest.clickActionData = data;
        this.dirty = true;
    }

    private void onHover(String name, String data) {
        MessagePart latest = this.latest();
        latest.hoverActionName = name;
        latest.hoverActionData = data;
        this.dirty = true;
    }
}

