package me.Hackusate_PvP.KitPvP.Utils;

import org.apache.commons.lang.time.DurationFormatUtils;

import java.text.DecimalFormat;
import java.util.concurrent.TimeUnit;

public class TimeUtils {
    public TimeUtils() {
    }

    public static DecimalFormat getDecimalFormat() {
        return new DecimalFormat("0.0");
    }

    public static long parse(String input) {
        if (input != null && !input.isEmpty()) {
            long result = 0L;
            StringBuilder number = new StringBuilder();

            for(int i = 0; i < input.length(); ++i) {
                char c = input.charAt(i);
                if (Character.isDigit(c)) {
                    number.append(c);
                } else {
                    String str;
                    if (Character.isLetter(c) && !(str = number.toString()).isEmpty()) {
                        result += convert(Integer.parseInt(str), c);
                        number = new StringBuilder();
                    }
                }
            }

            return result;
        } else {
            return -1L;
        }
    }

    private static long convert(int value, char unit) {
        switch(unit) {
            case 'M':
                return (long)value * TimeUnit.DAYS.toMillis(30L);
            case 'd':
                return (long)value * TimeUnit.DAYS.toMillis(1L);
            case 'h':
                return (long)value * TimeUnit.HOURS.toMillis(1L);
            case 'm':
                return (long)value * TimeUnit.MINUTES.toMillis(1L);
            case 's':
                return (long)value * TimeUnit.SECONDS.toMillis(1L);
            case 'y':
                return (long)value * TimeUnit.DAYS.toMillis(365L);
            default:
                return -1L;
        }
    }

    public static class IntegerCountdown {
        public IntegerCountdown() {
        }

        public static String setFormat(Integer value) {
            int remainder = value.intValue() * 1000;
            int seconds = remainder / 1000 % 60;
            int minutes = remainder / '\uea60' % 60;
            int hours = remainder / 3600000 % 24;
            return (hours > 0 ? String.format("%02d:", hours) : "") + String.format("%02d:%02d", minutes, seconds);
        }
    }

    public static class LongCountdown {
        public LongCountdown() {
        }

        public static String setFormat(Long value) {
            return value.longValue() < TimeUnit.MINUTES.toMillis(1L) ? TimeUtils.getDecimalFormat().format((double)value.longValue() / 1000.0D) + "s" : DurationFormatUtils.formatDuration(value.longValue(), (value.longValue() >= TimeUnit.HOURS.toMillis(1L) ? "HH:" : "") + "mm:ss");
        }
    }
}
