package me.Hackusate_PvP.KitPvP.Utils;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.logging.Level;

public class Settings {
    public static File configFile = new File("settings.yml");
    public static YamlConfiguration config;
    public static String HWID;

    public Settings() {
    }

    private static boolean getBoolean(String path, boolean def) {
        config.addDefault(path, def);
        return config.getBoolean(path, config.getBoolean(path));
    }

    private static int getInt(String path, int def) {
        config.addDefault(path, def);
        return config.getInt(path, config.getInt(path));
    }

    private static <T> List getList(String path, T def) {
        config.addDefault(path, def);
        return config.getList(path, config.getList(path));
    }

    private static String getString(String path, String def) {
        config.addDefault(path, def);
        return config.getString(path, config.getString(path));
    }

    private static double getDouble(String path, double def) {
        config.addDefault(path, def);
        return config.getDouble(path, config.getDouble(path));
    }

    public void init() {
        config = YamlConfiguration.loadConfiguration(configFile);
        config.options().copyDefaults(true);
        Method[] var1 = Settings.class.getDeclaredMethods();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            Method method = var1[var3];
            if (Modifier.isPrivate(method.getModifiers()) && method.getParameterTypes().length == 0 && method.getReturnType().equals(Void.TYPE)) {
                method.setAccessible(true);

                try {
                    method.invoke((Object)null);
                } catch (Exception var7) {
                    Bukkit.getLogger().log(Level.SEVERE, var7.getMessage());
                }

                HWID = getString("HWID", "");
            }
        }

        try {
            config.save(configFile);
        } catch (IOException var6) {
            Bukkit.getLogger().log(Level.SEVERE, "Unable to save default configuration file!");
        }

    }

    private static float getFloat(String path, float def) {
        return (float)getDouble(path, (double)def);
    }



    public static void saveConfig() {
        try {
            config.save(configFile);
        } catch (Exception var1) {
            System.out.print("Error occured while saving kitpvp config");
        }

    }
}

