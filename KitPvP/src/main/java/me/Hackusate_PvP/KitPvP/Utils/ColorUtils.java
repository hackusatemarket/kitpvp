package me.Hackusate_PvP.KitPvP.Utils;

import org.apache.commons.lang.StringEscapeUtils;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ColorUtils {
    public ColorUtils() {
    }

    public String translateFromString(String text) {
        return StringEscapeUtils.unescapeJava(ChatColor.translateAlternateColorCodes('&', text));
    }

    public List<String> translateFromArray(List<String> text) {
        List<String> messages = new ArrayList();
        Iterator var4 = text.iterator();

        while(var4.hasNext()) {
            String string = (String)var4.next();
            messages.add(this.translateFromString(string));
        }

        return messages;
    }
}

