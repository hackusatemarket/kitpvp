package me.Hackusate_PvP.KitPvP.Board;

import com.bizarrealex.aether.scoreboard.Board;
import com.bizarrealex.aether.scoreboard.BoardAdapter;
import com.bizarrealex.aether.scoreboard.cooldown.BoardCooldown;
import me.Hackusate_PvP.KitPvP.CoreAPI;
import me.Hackusate_PvP.KitPvP.Events.FFA.Commands.FFA;
import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BoardLink implements BoardAdapter, Listener {

    public BoardLink(JavaPlugin plugin) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @Override
    public String getTitle(Player player) {
        return ChatColor.translateAlternateColorCodes('&', Main.config.getString("Board.Title"));
    }

    @Override
    public List<String> getScoreboard(Player player, Board board, Set<BoardCooldown> cooldowns) {
        List<String> toReturn = new ArrayList<String>();
        if (FFA.ffastart == 1) {
            return getFormatLines(Main.config.getStringList("Board.FFA_START"), player);
        } else if (CoreAPI.start == 1) {
            return getFormatLines(Main.config.getStringList("Board.TDM_START"), player);
        } else if (CoreAPI.pvp == 1) {
            return getFormatLines(Main.config.getStringList("Board.FFA"), player);
        } else if (CoreAPI.tdm == 1) {
            return getFormatLines(Main.config.getStringList("Board.TDM"), player);
        } else {
            return getFormatLines(Main.config.getStringList("Board.Lines"), player);
        }
    }

    private List<String> getFormatLines(List<String> strings, Player player) {
        List<String> toReturn = new ArrayList<String>();

        for (String string : strings) {
            string = ChatColor.translateAlternateColorCodes('&', string);
            string = string.replace("%PLAYER%", player.getName() + "");
            string = string.replace("%RANK%", Main.getPermissions().getPrimaryGroup(player) + "");
            string = string.replace("%KILLS%", player.getStatistic(Statistic.PLAYER_KILLS) + "");
            string = string.replace("%DEATHS%", player.getStatistic(Statistic.DEATHS) + "");
            string = string.replace("%STREAK%", CoreAPI.getStreak(player) + "");
            string = string.replace("%FFAPLAYERS%", CoreAPI.getJoinedPlayer() + "");
            string = string.replace("%MAXFFAPLAYERS%", CoreAPI.getMaxPlayers() + "");
            string = string.replace("%ONLINE%", Bukkit.getOnlinePlayers().size() + "");
            toReturn.add(string);
        }

        return toReturn;
    }
}
