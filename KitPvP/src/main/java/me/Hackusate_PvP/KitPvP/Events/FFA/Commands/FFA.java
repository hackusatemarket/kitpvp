package me.Hackusate_PvP.KitPvP.Events.FFA.Commands;

import me.Hackusate_PvP.KitPvP.CoreAPI;
import me.Hackusate_PvP.KitPvP.Utils.Lang;
import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FFA implements CommandExecutor {
    public static int ffastart;


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {

        if (args.length == 0) {
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getMainColor() + "FFA " + Main.getAltColor() + "Help"));
            p.sendMessage("");
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "/ffa start " + Main.getAltColor() + "(&oStarts a ffa event" + Main.getAltColor() + ")"));
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + " " + Main.getMainColor() + "/ffa join " + Main.getAltColor() + "(&oJoins a ffa event" + Main.getAltColor() + ")"));
            p.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getBar()));
        } else {
            
            if (args[0].equalsIgnoreCase("DEV")) {
                p.sendMessage("Joined: " + CoreAPI.getJoinedPlayer());
            }

            if (Main.config.getBoolean("Event.FFA.Permission")) {
                if (p.hasPermission("core.command.ffa.start")) {
                    //p.sendMessage(ChatColor.RED + "Perm true");
                    if (args[0].equalsIgnoreCase("start") && args.length > 1) {
                        if (ffastart == 0) {
                            if (args[1].equalsIgnoreCase("NoDebuff")) {
                                ffastart = 1;
                                p.sendMessage(ChatColor.GREEN + "You have stated a FFA event.");
                                CoreAPI.kit = "NoDebuff";
                                for (Player player : Bukkit.getOnlinePlayers()) {
                                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + Main.getMainColor() + " FFA Event " + Main.getAltColor() + "has started " + Main.getMainColor() + "/ffa join"));
                                }
                            }
                        } else {
                            p.sendMessage(ChatColor.RED + "Event has already started.");
                        }
                    } else if (args[0].equalsIgnoreCase("start")) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&',Lang.INVALD_ARGS.toString()));
                    }
                } else {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.NO_PERMS.toString()));
                }
            } else {
                //  p.sendMessage(ChatColor.RED + "Perm false");
                if (args[0].equalsIgnoreCase("start") && args.length > 1) {
                    if (ffastart == 0) {
                        if (args[1].equalsIgnoreCase("NoDebuff")) {
                            ffastart = 1;
                            p.sendMessage(ChatColor.GREEN + "You have stated a FFA event.");
                            CoreAPI.kit = "NoDebuff";
                            for (Player player : Bukkit.getOnlinePlayers()) {
                                player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getOther() + Main.getSymbol() + Main.getMainColor() + " FFA Event " + Main.getAltColor() + "has started " + Main.getMainColor() + "/ffa join"));
                            }
                        }
                    } else {
                        p.sendMessage(ChatColor.RED + "Event has already started.");
                    }

                } else if (args[0].equalsIgnoreCase("start")) {
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&', Lang.INVALD_ARGS.toString()));
                }
            }


                if (args[0].equalsIgnoreCase("kit")) {
                p.sendMessage(CoreAPI.kit);
                }
                if (args[0].equalsIgnoreCase("join")) {
                    if (ffastart == 1) {
                        if (!(CoreAPI.isJoinedPlayer((Player) p))) {
                            if (CoreAPI.getJoinedPlayer() <= CoreAPI.getMaxPlayers()) {
                                CoreAPI.setJoined((Player) p);
                                for (Player player : Bukkit.getOnlinePlayers()) {
                                    if (CoreAPI.isJoinedPlayer(player)) {
                                        player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.getMainColor() + p.getName() + Main.getAltColor() + " has joined " + Main.getOther() + "(" + CoreAPI.getJoinedPlayer() + "/" + CoreAPI.getMaxPlayers() + ")"));

                                    }

                                }

                                p.sendMessage(ChatColor.GREEN + "You have joined.");
                            } else {
                                p.sendMessage(ChatColor.RED + "Event is full");
                            }
                        } else {
                                p.sendMessage(ChatColor.RED + "You have already joined.");
                            }
                    } else {
                        p.sendMessage(ChatColor.RED + "Event has already started.");
                    }
                }
            }

        return false;
    }

}
