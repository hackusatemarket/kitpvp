package me.Hackusate_PvP.KitPvP.Events.TDM.Listeners;

import me.Hackusate_PvP.KitPvP.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class TDMPatch implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onTeamDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        Player killer = event.getEntity().getKiller();
        if (CoreAPI.isTeam1(player)) {
            CoreAPI.team1.remove(player.getUniqueId());
            player.sendMessage(ChatColor.RED + "You have been eliminated.");
        } else if (CoreAPI.isTeam2(player)) {
            CoreAPI.team2.remove(player.getUniqueId());
            player.sendMessage(ChatColor.RED + "You have been eliminated.");
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onTeamDamange(EntityDamageEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) {
            Player player = (Player) event.getEntity();
            for (Player team : Bukkit.getOnlinePlayers()) {
                if (CoreAPI.isTeam1(team) && CoreAPI.isTeam1(player)) {
                    event.setCancelled(true);
                } else if (CoreAPI.isTeam2(team) && CoreAPI.isTeam2(player)) {
                    event.setCancelled(true);
                }
            }
        }
    }

  /*  @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) {
            if (CoreAPI.start == 0) {
                event.setCancelled(true);
            }
        }
    } */


}
