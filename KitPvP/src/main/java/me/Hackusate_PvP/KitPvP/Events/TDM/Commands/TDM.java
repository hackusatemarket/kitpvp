package me.Hackusate_PvP.KitPvP.Events.TDM.Commands;

import me.Hackusate_PvP.KitPvP.CoreAPI;
import me.Hackusate_PvP.KitPvP.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class TDM implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender p, Command command, String label, String[] args) {
        Player player = (Player) p;
        if (args.length == 0) {
            p.sendMessage(ChatColor.GRAY + ChatColor.STRIKETHROUGH.toString() + "----------------");
            p.sendMessage(ChatColor.GOLD + "TDM " + ChatColor.GRAY + "Help");
            p.sendMessage("");
            p.sendMessage(ChatColor.RED + "* " + ChatColor.GOLD + "/tdm " + ChatColor.GRAY + "(" + ChatColor.ITALIC + "Displays help page" + ChatColor.GRAY + ")");
            p.sendMessage(ChatColor.RED + "* " + ChatColor.GOLD + "/tdm start" + ChatColor.GRAY + "(" + ChatColor.ITALIC + "Starts the event" + ChatColor.GRAY + ")");
            p.sendMessage(ChatColor.RED + "* " + ChatColor.GOLD + "/tdm join" + ChatColor.GRAY + "(" + ChatColor.ITALIC + "Join the event" + ChatColor.GRAY + ")");
            p.sendMessage(ChatColor.GRAY + ChatColor.STRIKETHROUGH.toString() + "----------------");

        } else {
            if (args[0].equalsIgnoreCase("start")) {
                if (CoreAPI.start == 0) {
                    if (CoreAPI.getJoinedSize() >= 4) {
                        CoreAPI.start = 1;
                        for (Player online : Bukkit.getOnlinePlayers()) {
                            online.sendMessage(ChatColor.RED + "Event is starting in 10 seconds.");
                            new BukkitRunnable() {
                                public void run() {
                                    CoreAPI.tdm = 1;
                                }
                            }.runTaskTimer(Main.plugin, 10L, 0);

                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "Not enough players.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Event has already started.");
                }
            }

            if (args[0].equalsIgnoreCase("join")) {
                if (CoreAPI.start == 0) {
                    if (!(CoreAPI.isTeam1(player)) && !(CoreAPI.isTeam2(player))) {
                        if (CoreAPI.getJoinedSize() <= 16) {
                            if (CoreAPI.getJoinedSize() <= 8) {
                                CoreAPI.addTeam(player, 1);
                                player.sendMessage(ChatColor.GREEN + "You have been added to team 1");
                                for (Player team1 : Bukkit.getOnlinePlayers()) {
                                    if (CoreAPI.isTeam1(team1)) {
                                        team1.sendMessage(ChatColor.RED + p.getName() + ChatColor.GRAY + " has joined your team");
                                    }
                                }
                            } else if (CoreAPI.getJoinedSize() > 8) {
                                CoreAPI.addTeam(player, 2);
                                player.sendMessage(ChatColor.GREEN + "You have been added to team 2");
                                for (Player team2 : Bukkit.getOnlinePlayers()) {
                                    team2.sendMessage(ChatColor.RED + p.getName() + ChatColor.GRAY + " has joined your team.");
                                }
                            }
                        } else {
                            p.sendMessage(ChatColor.RED + "Event is full");
                        }
                    } else {
                        player.sendMessage(ChatColor.RED + "You are already on a team.");
                    }
                } else {
                    player.sendMessage(ChatColor.RED + "Event has already started.");
                }
            }

            if (args[0].equalsIgnoreCase("leave")) {
                if (CoreAPI.start == 0) {
                    if (CoreAPI.isTeam1(player)) {
                        CoreAPI.team1.remove(player.getUniqueId());
                        player.sendMessage(ChatColor.RED + "You have left.");
                        for (Player team1 : Bukkit.getOnlinePlayers()) {
                            if (CoreAPI.isTeam1(team1)) {
                                team1.sendMessage(ChatColor.RED + player.getName() + ChatColor.GRAY + " has left your team.");
                            }
                        }
                    } else if (CoreAPI.isTeam2(player)) {
                        CoreAPI.team2.remove(player.getUniqueId());
                        player.sendMessage(ChatColor.RED + "You have left.");
                        for (Player team2 : Bukkit.getOnlinePlayers()) {
                            if (CoreAPI.isTeam2(team2)) {
                                team2.sendMessage(ChatColor.RED + p.getName() + ChatColor.GRAY + " has left your team.");
                            }
                        }
                    }
                } else {
                    p.sendMessage(ChatColor.RED + "You cannot leave an event that has started.");
                }
            }
        }

        return false;
    }
}
