package me.Hackusate_PvP.KitPvP.Events.FFA.Listeners;

import me.Hackusate_PvP.KitPvP.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class FFApatch implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPvP(EntityDamageEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) {
            if (CoreAPI.pvp == 1) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (CoreAPI.isJoinedPlayer(player)) {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void pvpmove(PlayerMoveEvent event) {
        Location from = event.getFrom();
        Location to = event.getTo();
        if (CoreAPI.pvp == 1) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (CoreAPI.isJoinedPlayer(player)) {
                    if (from.getBlockX() != to.getBlockX() || from.getBlockZ() != to.getBlockZ()) {
                        event.setTo(from);
                    }
                }
            }
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onffaDeath(PlayerDeathEvent event) {
        Player player = event.getEntity();
        Player killer = event.getEntity().getKiller();
        for (Player online : Bukkit.getOnlinePlayers()) {
            if (CoreAPI.isJoinedPlayer(online)) {
                player.sendMessage(ChatColor.RED + "You have been eliminated by " + ChatColor.BOLD + killer.getName());
                killer.sendMessage(ChatColor.GREEN + "You have eliminated " + ChatColor.BOLD + player.getName());
                CoreAPI.getJoinedPlayers().remove(player.getUniqueId());
                if (CoreAPI.getJoinedPlayer() == 1) {
                    player.sendMessage(ChatColor.RED + "You have been eliminated by " + ChatColor.BOLD + killer.getName());
                    killer.sendMessage(ChatColor.GREEN + "You have won!");
                    CoreAPI.setWiiner(killer);
                    Bukkit.broadcastMessage(ChatColor.DARK_RED + killer.getName() + ChatColor.GOLD + " has won the event!");
                }
            }
        }
    }

}
